import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import { BrowserRouter } from 'react-router-dom';
import HomeContainer from './HomeContainer';

function App() {
  return (
      <BrowserRouter>
          <div className="App">
            <HomeContainer/>
          </div>
      </BrowserRouter>
    );
}

export default App;
