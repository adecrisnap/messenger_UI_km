import React from 'react';
import PageContainer from './components/PageContainer';
import AppBar from './components/AppBar';
import { Redirect } from 'react-router';

const HomeContainer = props => {
    return (
        <React.Fragment>
            <AppBar>
                <PageContainer/>
                <Redirect to='/home' />
                
            </AppBar>
        </React.Fragment>
    )
}

export default HomeContainer

// backgroundColor: "#f5f5f5",

/*
<StickyFooter
                    bottomThreshold={50}
                    normalStyles={{
                        padding: "2rem"
                    }}
                    stickyStyles={{
                        backgroundColor: "rgba(255,255,255,.8)",
                        padding: "2rem"
                    }}
                >
                    <div align='center'>
                        <Label as='a' basic>
                            All rights reserved {new Date().getFullYear()}
                        </Label>
                    </div>
                </StickyFooter>
                */