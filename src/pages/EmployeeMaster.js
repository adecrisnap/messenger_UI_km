import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import usePutData from '../services/usePutData';
import SimpleForm from '../components/SimpleForm'
import URLLocation from '../services/URLLocation';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import { useHistory } from "react-router-dom";
import Helper from '../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const EmployeeMaster = props => {
    let history = useHistory();
    const [ idstate,setIdState] = useState(null)
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );

    useEffect(()=>{
        setAction('get');
        setGetUrl(URLLocation.getUrl() + '/getemployeemaster','')
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        ) 
        {
            
            var elem = [];
            var km_employee_master = null;
            var lastmodified = null;
            
            if (action==='get') {
                setIdState(stateGet.data[0]._id);
                km_employee_master = stateGet.data[0].km_employee_master;
                lastmodified = stateGet.data[0].lastmodified;
            }
            if (action==='put') {
                km_employee_master = elements[0].value;
                lastmodified = elements[1].value;
            }

            elem.push({
                type:'checkbox',
                name:'km_employee_master',
                label:'Non KMTR employee master ?',
                value:km_employee_master, 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'lastmodified',
                label:'Last Modified',
                value:lastmodified, 
            })

            setElements(elem);
            if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePut.randomstatus])

    const onYes = () => {
        var data = {
            km_employee_master : elements[0].value,
        };
        setPutload(data);
        setAction('put')
        setPutUrl(URLLocation.getUrl() + '/setemployeemaster/' + idstate);
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
            <HeaderEnhancedx title='Employee Master Setting' text='Employee master setting' />
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={1}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah setting employee master'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default EmployeeMaster;