import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import GridTable from '../../components/GridTable'
import URLLocation from '../../services/URLLocation';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const HRMembers = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );
    const {
        id,
        mode
    } = props
    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getemployeesbyrole/' + id);
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : 'nama', columncaption : 'Name', dataType: 'String', width: 12});
            columns.push({columnname : 'nik', columncaption : 'NIK', dataType: 'String', width: 3});
            columns.push({columnname : 'posst', columncaption : 'Posisi', dataType: 'String', width: 15});
            columns.push({columnname : 'email', columncaption : 'E-mail', dataType: 'String', width: 15});
            columns.push({columnname : 'gsber', columncaption : 'Pabrik', dataType: 'String', width: 3});
            columns.push({columnname : 'ho', columncaption : 'Head Office ?', dataType: 'Custom', width: 5, customvalues : [ {value : 'y', text : 'Yes'}, {value: 'n', text :'No'}]});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        sortedby={'nama'}
                        ascdesc={'ascending'}
                        showaddnew={false}
                        showaddnewonempty={false}
                        emptycaption="You don't have any HR member"
                        emptyimage={null}
                        emptytitle="No HR member"
                    />
                )
            }
        </Container>
    )
}

export default HRMembers;

/*
 (stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No HR Member"
                            caption="You don't have any HR Member"
                            showButton={false}
                        />
                    )
                    : 
                    (



*/