import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import LoadingBox2 from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';


const RoleDetail = props => {
    let history = useHistory();
    const {
        id,
        mode
    } = props
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const [optionsX,setOptionsX] = useState([]);
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );
    const [openload, setOpenLoad] = useState(false);
    
    useEffect(()=>{
        var options = [];
        const apiUrl = URLLocation.getUrl() + '/getservers/1';
        setOpenLoad(true);
        fetch(apiUrl)
        .then((response) => response.json()
        .then((data) => 
            {
                data.map((obj)=>{
                    options.push({
                        value : obj._id,
                        text : obj.servername
                    })
                })
                setOptionsX(options);
                setOpenLoad(false)
                    
                if (modestate==='edit') {
                    setAction('get');
                    setGetUrl(URLLocation.getUrl() + '/getrolebyid/' + id,'')
                }
                else {
                    var elem = [];
                    elem.push({
                        type:'text',
                        name:'rolename',
                        label:'Role Name',
                        placeholder:'Role Name',
                        value:'', 
                    })
        
                    elem.push({
                        type:'dropdown',
                        name:'Server',
                        label:'Server',
                        multiple:false,
                        value:0, 
                        options:options
                    })
        
                    elem.push({
                        type:'text',
                        name:'keyword',
                        label:'Keyword',
                        placeholder:'Keyword',
                        value:'', 
                    })
        
                    
                    elem.push({
                        type:'checkbox',
                        name:'is_active',
                        label:'Is Active',
                        value: 1, 
                    })
        
                    elem.push({
                        type:'checkbox',
                        name:'activateExclude',
                        label:'Activate exclude HR Members',
                        value: 1, 
                    })

                    elem.push({
                        type:'checkbox',
                        name:'nonkmdb',
                        label:'Non KM database',
                        value:1, 
                    })
        
                    elem.push({
                        type:'label',
                        datatype:'DATETIME',
                        name:'lastmodified',
                        label:'Last Modified',
                        value: new Date(),
                    })
                    setElements(elem);
                }
            }
        ))
        .catch((err)=>{
            setOpenLoad(false);
            NotificationManager.error(err, 'Error', 3000);
        })
       
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
            ) 
        {
            var idtemp = id;   
            var elem = [];
            var rolename = null;
            var dbserver = null;
            var keyword = null;
            var is_active = null;
            var activateExclude = null;
            var lastmodified = null;
            var nonkmdb = null;

            if (action==='get') {
                rolename = stateGet.data[0].rolename; 
                dbserver = stateGet.data[0].dbserver;
                keyword = stateGet.data[0].keyword;
                activateExclude = stateGet.data[0].activateExclude;
                is_active = stateGet.data[0].is_active;
                lastmodified = stateGet.data[0].lastmodified;
                nonkmdb = stateGet.data[0].nonkmdb;
            }
            if (action==='put') {
                rolename = elements[0].value; 
                dbserver = elements[1].value;
                keyword = elements[2].value;
                activateExclude = elements[4].value;
                is_active = elements[3].value;
                lastmodified = elements[6].value;
                nonkmdb = elements[5].value;
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
                rolename = elements[0].value; 
                dbserver = elements[1].value;
                keyword = elements[2].value;
                activateExclude = elements[4].value;
                is_active = elements[3].value;
                lastmodified = elements[6].value;
                nonkmdb = elements[5].value;
            }

            elem.push({
                type:'text',
                name:'rolename',
                label:'Role Name',
                placeholder:'Role Name',
                value:rolename, 
            })

            elem.push({
                type:'dropdown',
                name:'Server',
                label:'Server',
                multiple:false,
                value:dbserver, 
                options:optionsX
            })

            elem.push({
                type:'text',
                name:'keyword',
                label:'Keyword',
                placeholder:'Keyword',
                value:keyword, 
            })
            

            elem.push({
                type:'checkbox',
                name:'is_active',
                label:'Is Active',
                value:is_active, 
            })

            elem.push({
                type:'checkbox',
                name:'activateExclude',
                label:'Activate exclude HR Members',
                value:activateExclude, 
            })

            elem.push({
                type:'checkbox',
                name:'nonkmdb',
                label:'Non KM database',
                value:nonkmdb, 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'lastmodified',
                label:'Last Modified',
                value:lastmodified, 
            })

            setIDState(idtemp);
            setElements(elem);
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onClose = () => {
        history.goBack();
    } 

    const onYes = () => {
        var data = {
            rolename : elements[0].value,
            dbserver : elements[1].value,
            keyword : elements[2].value,
            is_active : elements[3].value,
            activateExclude : elements[4].value,
            nonkmdb : elements[5].value,
            lastmodified : elements[6].value

        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/editrole/' + idstate);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createrole');
        }
        
    }

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
             <NotificationContainer/>
             <LoadingBox2 open={openload}/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                   
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={1}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail role di mana role menentukan penerima e-mail/pesan. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>    
                )
            }
        </Container>
    )
}

export default RoleDetail;