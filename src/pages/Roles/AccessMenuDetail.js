import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import LoadingBox2 from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePutData from '../../services/usePutData';
import usePostData from '../../services/usePostData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import HeaderEnhancedx from '../../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const AccessMenuDetail = props => {
    let history = useHistory();
    const {
        id,
        roleid,
        mode
    } = props.match.params
    const [ openload, setOpenLoad] = useState(false)
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );

    useEffect(()=>{
        var elem = [];
        var options1 = [];
        const apiUrl = URLLocation.getUrl() + '/getapps/1';
        setOpenLoad(true);
        fetch(apiUrl)
        .then((response) => response.json()
        .then((data) => 
            {
                data.map((obj)=>{
                    options1.push({
                        value :obj._id,
                        text : obj.appname 
                    })
                })
                elem.push({
                    type:'dropdown',
                    name:'report_appid',
                    label:'Application',
                    multiple:false,
                    value:0, 
                    options:options1
                })     
                elem.push({
                    type:'dropdown',
                    name:'menus',
                    label:'Menu',
                    multiple:true,
                    value:null, 
                    options:null
                }) 
                elem.push({
                    type:'label',
                    datatype:'DATETIME',
                    name:'lastmodified',
                    label:'Last modified',
                    value: new Date(),
                })
                
                setOpenLoad(false)
                if (modestate==='edit') {
                    setAction('get');
                    setGetUrl(URLLocation.getUrl() + '/getrolemenusdetail/' + id,'')
                }
                else {
                    setModeState('add');
                    setElements(elem);
                }
            }
        ))
        .catch((err)=>{
            setOpenLoad(false);
            NotificationManager.error(err, 'Error', 3000);
        })
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        ) 
        {
            var elem = [];
            var report_appid = null;
            var menus = [];
            var lastmodified = null;
        
            if (action==='get') {
                report_appid = stateGet.data[0].report_appid;
                for (var i=0;i<stateGet.data[0].menus.length;i++) {
                    menus.push(stateGet.data[0].menus[i].value)
                }
                lastmodified = stateGet.data[0].lastmodified;
            }
            if (action==='put') {
                report_appid = elements[0].value; 
                menus = elements[1].value;
                lastmodified = elements[2].value;
            }
            if (action==='post') {
                setModeState('edit');
                report_appid = elements[0].value; 
                menus = elements[1].value;
                lastmodified = elements[2].value;
            }
            
            var elem = [];
            var options1 = [];
            var options2 = [];
            const apiUrl = URLLocation.getUrl() + '/getapps/1';
            setOpenLoad(true);
            fetch(apiUrl)
            .then((response) => response.json()
            .then((data) => 
                {
                    data.map((obj)=>{
                        options1.push({
                            value :obj._id,
                            text : obj.appname 
                        })
                    })
                    const apiUrl2 = URLLocation.getUrl() + '/getmenusbyapp/' + report_appid;
                    fetch(apiUrl2)
                    .then((response) => response.json())
                    .then((data) => {
                        data.map((obj)=>{
                            options2.push({
                                value :obj.Report_ID + '|' + obj.Report_AppID + '|' + obj.Report_Name,
                                text : obj.Report_Name  
                            })
                        })
                        elem.push({
                            type:'dropdown',
                            name:'report_appid',
                            label:'Application',
                            multiple:false,
                            value:report_appid, 
                            options:options1
                        })     
                        elem.push({
                            type:'dropdown',
                            name:'menus',
                            label:'Menu',
                            multiple:true,
                            value:menus, 
                            options:options2
                        }) 
                        elem.push({
                            type:'label',
                            datatype:'DATETIME',
                            name:'lastmodified',
                            label:'Last modified',
                            value: lastmodified,
                        })
                        setOpenLoad(false)
                        setElements(elem);
                        if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
                    }); 
                }
            ))
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const findTextByValue = (options,value) => {
        var i = 0;
        var found = false;
        var text = '';
        while(i<options.length && found===false) {
            if (options[i].value===value) {
                found = true;
                text = options[i].text;
            }
            i++
        }
        return text;
    }

    const onYes = () => {
        var data = {
            role_id : roleid,
            report_appid : elements[0].value,
            app_name : findTextByValue(elements[0].options,elements[0].value),
            menu_ids : elements[1].value
        };
        //alert(JSON.stringify(data))
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/setrolemenus/' + id);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createrolemenus');
        }
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var options = [];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        if (name==='report_appid') {
            const apiUrl2 = URLLocation.getUrl() + '/getmenusbyapp/' + value;
            fetch(apiUrl2)
            .then((response) => response.json()
            .then((data) => {
                data.map((obj)=>{
                    options.push({
                        value :obj.Report_ID + '|' + obj.Report_AppID + '|' + obj.Report_Name,
                        text : obj.Report_Name 
                    })
                })
                var idx = Helper.findIndex(elems,'name','menus');
                elems[idx].options = options;
                setElements(elems);
            }))
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
        }
        else {
            setElements(elems)
        }
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    const onDateChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].selectedDate = value;
        setElements(elems);
    }


    return (
        <Container>
            <HeaderEnhancedx title='Access Menus' text='Access menus' />
            <LoadingBox2 open={openload}/>
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={1}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onDateChange={onDateChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail akses menu'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default AccessMenuDetail;