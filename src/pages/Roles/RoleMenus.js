import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import GridTable from '../../components/GridTable'
import URLLocation from '../../services/URLLocation';
import {NotificationContainer, NotificationManager} from 'react-notifications';

const RoleMenus = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl} = useGetData(
        null,''
    );
    const {
        id,
        mode
    } = props
    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getrolemenus/' + id);
    },[])
   
    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : '_id', columncaption : '', dataType : 'String',  width : 3, link : 'accessmenudetail/edit/' + id, visible : false})
            columns.push({columnname : 'app_name', columncaption : 'Nama Aplikasi', dataType: 'String', width: 12});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 5});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])


    return (
        <Container>
            <NotificationContainer/>
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        sortedby={'nama'}
                        linkinrowlevel={true}
                        ascdesc={'ascending'}
                        showaddnew={true}
                        link={'/accessmenudetail/add/' + id + '/0'}
                        showaddnewonempty={false}
                        emptycaption="You don't have any access menus"
                        emptyimage={null}
                        emptytitle="No menu"
                        emptybuttoncaption="Add new access menu"
                    />
                )
            }
        </Container>
    )
}

export default RoleMenus;