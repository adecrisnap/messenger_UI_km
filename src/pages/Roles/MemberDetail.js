import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import LoadingBox2 from '../../components/LoadingBox';
import usePostData from '../../services/usePostData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import HeaderEnhancedx from '../../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const MemberDetail = props => {
    let history = useHistory();
    const {
        id,
        mode
    } = props.match.params
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [ nonkmdb, setNonkmdb ] = useState(0);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    //const [optionsEmp,setOptionsEmp] = useState([]);
    //const [optionsPlant,setOptionsPlant] = useState([]);
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const [ openload, setOpenLoad] = useState(false)
    
    useEffect(()=>{
        var options1 = [];
        var options2 = [];
        var elem = [];
        if (mode==='add') {
            const apiUrl = URLLocation.getUrl() + '/getemployees2/' + id;
            setOpenLoad(true);
            fetch(apiUrl)
            .then(r => 
                r.json()
            .then((data) => 
                {
                    if (data.nonkmdb===0)
                        data.data.map((obj)=>{
                            options1.push({
                                value :obj.nik,
                                text : obj.nama + ' : ' + obj.nik + ' - ' + obj.posst 
                            })
                        })
                    else 
                        data.data.map((obj)=>{
                            options1.push({
                                value :obj.nik,
                                text : obj.nama + ' : ' + obj.nik + ' - ' + obj.posisi 
                            })
                        })
                    const apiUrl2 = URLLocation.getUrl() + '/getplants';
                    fetch(apiUrl2)
                    .then((response) => response.json()
                    .then((data) => {
                        data.map((obj)=>{
                            options2.push({
                                value :obj.name_coy,
                                text : obj.name_coy 
                            })
                        })
                        elem.push({
                            type:'dropdown',
                            name:'Employee',
                            label:'Employee',
                            multiple:false,
                            value:0, 
                            options:options1
                        })     
                        elem.push({
                            type:'dropdown',
                            name:'Plants',
                            label:'Plants',
                            multiple:true,
                            value:0, 
                            options:options2
                        })     
                        setNonkmdb(nonkmdb);
                        setOpenLoad(false)
                        setModeState('add');
                        setIDState(id);
                        setElements(elem);
                    }))
                    .catch((err)=>{
                        setOpenLoad(false);
                        NotificationManager.error(err, 'Error', 3000);
                    })
                }
            ))
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })

            
        }
    },[])

    useEffect(()=>{
        if ((action==='post' && (statePost.status===200 || statePost.status===201))) 
        {
            history.goBack();
        }
        else {
            var msg = '';
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[statePost.randomstatus])

    const onClose = () => {
        history.goBack();
    } 

    const onYes = () => {
        if (modestate==='add') {
            var dt = {}
            var pload = {
                roleid : idstate,
                id : elements[0].value,
                nonkmdb : nonkmdb
            }
            const apiUrl = URLLocation.getUrl() + '/getemployeebyid/';
            setOpenLoad(true)
            fetch(apiUrl, {
                method: 'POST', // or 'PUT'
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify(pload),
            })
            .then((response) => response.json()
            .then((data) => 
                {
                    if (nonkmdb===0) {
                        dt = {
                            roleid : id,
                            id_karyawan : data[0].id_karyawan,
                            nik : data[0].nik,
                            nama : data[0].nama,
                            posisi : data[0].posst,
                            ho : data[0].ho,
                            pabrik : data[0].gsber,
                            email : data[0].email,
                            plantselected : elements[1].value,
                            divisi : data[0].divisi,
                            dept : data[0].dept
                        }
                    }
                    else {
                        dt = {
                            roleid : id,
                            id_karyawan : data[0].nik,
                            nik : data[0].nik,
                            nama : data[0].nama,
                            posisi : data[0].posisi,
                            ho : '',
                            pabrik : data[0].divisi,
                            email : data[0].email,
                            plantselected : elements[1].value,
                            divisi : data[0].divisi,
                            dept : ''
                        }
                    }
                    setOpenLoad(false)
                    setPayload(dt);
                    setAction('post');
                    setPostUrl(URLLocation.getUrl() + '/createrolemember');
                }))
                .catch((err)=>{
                    setOpenLoad(false);
                    NotificationManager.error(err, 'Error', 3000);
                })
              
        }
    }

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
            <HeaderEnhancedx title='Role Member' text='Detail Role Member' />
            <LoadingBox2 open={openload} />
            <NotificationContainer/>
            {(statePost.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                   
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={1}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={(mode==='add') ? 'Tambah member baru' : 'Hapus dari member ?'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>    
                
                )
            }
        </Container>
    )
}

export default MemberDetail;