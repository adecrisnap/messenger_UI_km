import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import ConfirmationBox from '../../components/ConfirmationBox';
import useGetData from '../../services/useGetData';
import GridTable from '../../components/GridTable'
import URLLocation from '../../services/URLLocation';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import DeleteDataToServer from '../../services/useDeleteData';


const Members = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const [idtodelete,setIdtodelete] = useState(null)
    const [openconf, setOpenconf] = useState(false)
    const  { stateGet,setGetUrl, setRefresh } = useGetData(
        null,''
    );
    const {
        id,
        mode
    } = props
    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getrolemembers/' + id);
    },[])
    const  { stateDelete, setDeleteUrl } = DeleteDataToServer(
        null,''
    );

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : '_id', columncaption : '', dataType : 'Button',  width : 4, action : 'delete', buttonCaption : 'Delete', visible : true})
            columns.push({columnname : 'nama', columncaption : 'Nama', dataType: 'String', width: 12});
            columns.push({columnname : 'nik', columncaption : 'NIK', dataType: 'String', width: 3});
            columns.push({columnname : 'email', columncaption : 'E-mail', dataType: 'String', width: 15});
            columns.push({columnname : 'gsber', columncaption : 'Pabrik', dataType: 'String', width: 3});
            columns.push({columnname : 'ho', columncaption : 'Head Office ?', dataType: 'Custom', width: 5, customvalues : [ {value : 'y', text : 'Yes'}, {value: 'n', text :'No'}]});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    useEffect(()=>{
        if (stateDelete.status===200 || stateDelete.status===201) {
           setRefresh(true)
        }
        else {
            var msg = '';
            msg = stateDelete.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateDelete.randomstatus])

    const buttonClick = (value,action) =>{
        setIdtodelete(value);
        setOpenconf(true)
    }

    const handleCancel = () => {
        setOpenconf(false)
    }

    const handleConfirm = () => {
        setOpenconf(false)
        setDeleteUrl(URLLocation.getUrl() + '/deleterolemember/' + idtodelete);
    }

    return (
        <Container>
            <NotificationContainer/>
            <ConfirmationBox message='Are you sure you want to delete ?' open={openconf} onYes={handleConfirm} onNo={handleCancel}/>
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        internalclick={false}
                        sortedby={'nama'}
                        linkinrowlevel={false}
                        ascdesc={'ascending'}
                        showaddnew={true}
                        link={'/memberdetail/add/' + id}
                        buttonClick={buttonClick}
                        showaddnewonempty={false}
                        emptycaption="You don't have any member"
                        emptyimage={null}
                        emptytitle="No member"
                        emptybuttoncaption="Add new member"
                    />
                )
            }
        </Container>
    )
}

export default Members;

/*

                    (stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No member"
                            caption="You don't have any member"
                            showButton={true}
                            buttonCaption={'Add new'}
                            onClick={handleClick} 
                        />
                    )
                    : 
                    (

                        */