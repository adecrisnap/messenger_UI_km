import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import ConfirmationBox from '../../components/ConfirmationBox';
import useGetData from '../../services/useGetData';
import DeleteDataToServer from '../../services/useDeleteData';
import GridTable from '../../components/GridTable'
import URLLocation from '../../services/URLLocation';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { useHistory } from "react-router-dom";


const AppMenus = props => {
    let history = useHistory();
    const [dataSource,setDataSource] = useState(null);
    const [idtodelete,setIdtodelete] = useState(null)
    const [openconf, setOpenconf] = useState(false)
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl,setRefresh } = useGetData(
        null,''
    );
    const  { stateDelete, setDeleteUrl } = DeleteDataToServer(
        null,''
    );
    const {
        id,
    } = props

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getmenusbyapp/' + id);
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : 'Report_ID', columncaption : '', dataType : 'String',  width : 3, link : 'menudetail/edit/' + id, visible : false})
            //columns.push({columnname : 'Report_ID', columncaption : '', dataType : 'Button',  width : 3, action : 'delete', buttonCaption : 'Delete', visible : true})
            columns.push({columnname : 'Report_ID', columncaption : 'ID', dataType: 'String', width: 2});
            columns.push({columnname : 'Report_Name', columncaption : 'Report Name', dataType: 'String', width: 9});
            columns.push({columnname : 'Report_CategoryID', columncaption : 'Category', dataType: 'String', width: 3});
            columns.push({columnname : 'Report_Link', columncaption : 'Link', dataType: 'String', width: 9});
            columns.push({columnname : 'Report_Proc', columncaption : 'Stored Procedure', dataType: 'String', width: 9});
            columns.push({columnname : 'Report_Status', columncaption : 'Is Active', dataType: 'Custom', width: 3, customvalues : [ {value : 'A', text : 'Yes'}, {value: 'N', text :'No'}]});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    useEffect(()=>{
        if (stateDelete.status===200 || stateDelete.status===201) {
           setRefresh(true)
        }
        else {
            var msg = '';
            msg = stateDelete.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateDelete.randomstatus])

    const buttonClick = (value,action) =>{
        if (action==='delete') {
            setIdtodelete(value);
            setOpenconf(true)
        }
        else {
            history.push('/menudetail/edit/' + id + '/' + value)
        }
    }

    const handleCancel = () => {
        setOpenconf(false)
    }

    const handleConfirm = (inputText) => {
        setOpenconf(false)
        setDeleteUrl(URLLocation.getUrl() + '/deletecolumn/' + idtodelete);
    }

    return (
        <Container>
            <NotificationContainer/>
            <ConfirmationBox 
                message='Are you sure you want to delete ?' 
                open={openconf} 
                onYes={handleConfirm} 
                onNo={handleCancel}
            />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        sortedby={'topicname'}
                        ascdesc={'ascending'}

                        linkinrowlevel={true}
                        internalclick={false}
                        buttonClick={buttonClick}

                        link={'/menudetail/add/' + id + '/0'}
                        showaddnew={true}
                        showaddnewonempty={false}
                        emptycaption="You don't have any topic"
                        emptyimage={null}
                        emptytitle="No topic"
                    />
                )
            }
        </Container>
    )
}

export default AppMenus;

