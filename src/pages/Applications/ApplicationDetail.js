import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import LoadingBox2 from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const ApplicationDetail = props => {
    let history = useHistory();
    const {
        id,
        mode
    } = props
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const [optionsX,setOptionsX] = useState([]);
    const [openload, setOpenLoad] = useState(false);
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );

    useEffect(()=>{
        var options = [];
        const apiUrl = URLLocation.getUrl() + '/getservers/1';
        setOpenLoad(true);
        fetch(apiUrl)
        .then((response) => response.json()
        .then((data) => 
            {
                data.map((obj)=>{
                    options.push({
                        value : obj._id,
                        text : obj.servername
                    })
                })
                setOptionsX(options);
                setOpenLoad(false)
                if (modestate==='edit') {
                    setAction('get');
                    setGetUrl(URLLocation.getUrl() + '/getappbyid/' + id,'')
                }
                else {
                    var elem = [];
                    elem.push({
                        type:'text',
                        name:'appname',
                        label:'Application Name',
                        placeholder:'Application Name',
                        value:'', 
                    })

                    elem.push({
                        type:'text',
                        name:'appcode',
                        label:'Application Code',
                        placeholder:'Application Code',
                        value:'', 
                    })

                    elem.push({
                        type:'text',
                        name:'appdesc',
                        label:'Description',
                        placeholder:'Description',
                        value:'', 
                    })

                    elem.push({
                        type:'dropdown',
                        name:'Server',
                        label:'Server',
                        multiple:false,
                        value:0, 
                        options:options
                    })

                
                    elem.push({
                        type:'checkbox',
                        name:'is_active',
                        label:'Is Active',
                        value: 1, 
                    })

                    elem.push({
                        type:'label',
                        datatype:'DATETIME',
                        name:'lastmodified',
                        label:'Last Modified',
                        value: new Date(),
                    })

                    setElements(elem);
                }
        }))
        .catch((err)=>{
            setOpenLoad(false);
            NotificationManager.error(err, 'Error', 3000);
        })
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        ) 
        {
            var idtemp = id;   
            var elem = [];
            var appname = null;
            var appcode = null;
            var dbserver = null;
            var appdesc = null;
            var is_active = null;
            var lastmodified = null;

            
            if (action==='get') {
                appname = stateGet.data[0].appname; 
                appcode = stateGet.data[0].appcode;
                appdesc = stateGet.data[0].appdesc;
                dbserver = stateGet.data[0].dbserver;
                is_active = stateGet.data[0].is_active;
                lastmodified = stateGet.data[0].lastmodified;
            }
            if (action==='put') {
                appname = elements[0].value; 
                appcode = elements[1].value;
                appdesc = elements[2].value;
                dbserver = elements[3].value;
                is_active = elements[4].value;
                lastmodified = elements[5].value;
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
                appname = elements[0].value; 
                appcode = elements[1].value;
                appdesc = elements[2].value;
                dbserver = elements[3].value;
                is_active = elements[4].value;
                lastmodified = elements[5].value;
            }
            
            elem.push({
                type:'text',
                name:'appname',
                label:'Application Name',
                placeholder:'Application Name',
                value:appname, 
            })

            elem.push({
                type:'text',
                name:'appcode',
                label:'Application Code',
                placeholder:'Application Code',
                value:appcode, 
            })

            elem.push({
                type:'text',
                name:'appdesc',
                label:'Description',
                placeholder:'Description',
                value:appdesc, 
            })

            elem.push({
                type:'dropdown',
                name:'Server',
                label:'Server',
                multiple:false,
                value:dbserver, 
                options:optionsX
            })

            elem.push({
                type:'checkbox',
                name:'is_active',
                label:'Is Active',
                value:is_active, 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'lastmodified',
                label:'Last Modified',
                value:lastmodified, 
            })

            setIDState(idtemp);
            setElements(elem);
            if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onYes = () => {
        var data = {
            appname : elements[0].value,
            appcode : elements[1].value,
            appdesc : elements[2].value,
            dbserver : elements[3].value,
            is_active : elements[4].value
        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/editapp/' + idstate);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createapp');
        }
        
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
            <NotificationContainer/>
            <LoadingBox2 open={openload}/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={1}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail aplikasi yang terdaftar dengan aplikasi messenger ini. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default ApplicationDetail;

