import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import { useHistory } from "react-router-dom";
import HeaderEnhancedx from '../../components/HeaderEnhancedx';
import Helper from '../../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';

const MenuDetail = props => {
    let history = useHistory();
    const {
        appid,
        menuid,
        mode
    } = props.match.params
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );
    
    useEffect(()=>{
        if (modestate==='edit') {
            setAction('get');
            setGetUrl(URLLocation.getUrl() + '/getmenubyid/' + appid + '/' + menuid,'')
        }
        else {
            var elem = [];
            elem.push({
                type:'text',
                name:'Report_ID',
                label:'ID',
                placeholder:'ID',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'Report_Name',
                label:'Report Name',
                placeholder:'Report Name',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'Report_CategoryID',
                label:'Category',
                placeholder:'Category',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'Report_Link',
                label:'Link',
                placeholder:'Link',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'Report_Proc',
                label:'Stored Procedure',
                placeholder:'Stored Procedure',
                value:'', 
            })

            elem.push({
                type:'checkbox',
                name:'Report_Status',
                label:'Is Active',
                value: 1, 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'Report_CreateDate',
                label:'Created Date',
                value: new Date(),
            })
            setElements(elem);
        }
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
            ) 
        {
            var elem = [];
            var report_id = null;
            var report_name = null;
            var report_category = null;
            var report_link = null;
            var report_proc = null;
            var report_status = null;
            var lastmodified = null;

            if (action==='get') {
               report_id = stateGet.data[0].Report_ID;
               report_name = stateGet.data[0].Report_Name;
               report_category = stateGet.data[0].Report_CategoryID;
               report_link = stateGet.data[0].Report_Link;
               report_proc = stateGet.data[0].Report_Proc;
               report_status = stateGet.data[0].Report_Status;
               lastmodified = stateGet.data[0].Report_CreateDate;
            }
            if (action==='put') {
                report_id = elements[0].value;
                report_name = elements[1].value;
                report_category = elements[2].value; 
                report_link = elements[3].value;
                report_proc = elements[4].value;
                report_status = elements[5].value;
                lastmodified = elements[6].value;
            }
            if (action==='post') {
                setModeState('edit');
                report_id = elements[0].value;
                report_name = elements[1].value;
                report_category = elements[2].value; 
                report_link = elements[3].value;
                report_proc = elements[4].value;
                report_status = elements[5].value;
                lastmodified = elements[6].value;
            }

            elem.push({
                type:'text',
                name:'Report_ID',
                label:'ID',
                placeholder:'ID',
                value:report_id, 
            })

            elem.push({
                type:'text',
                name:'Report_Name',
                label:'Report Name',
                placeholder:'Report Name',
                value:report_name, 
            })

            elem.push({
                type:'text',
                name:'Report_CategoryID',
                label:'Category',
                placeholder:'Category',
                value:report_category, 
            })

            elem.push({
                type:'text',
                name:'Report_Link',
                label:'Link',
                placeholder:'Link',
                value:report_link, 
            })

            elem.push({
                type:'text',
                name:'Report_Proc',
                label:'Stored Procedure',
                placeholder:'Stored Procedure',
                value:report_proc, 
            })

            elem.push({
                type:'checkbox',
                name:'Report_Status',
                label:'Is Active',
                value: (report_status==='A' ? 1 : 0), 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'Report_CreateDate',
                label:'Created Date',
                value: lastmodified,
            })

            setElements(elem);
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onClose = () => {
        history.goBack();
    } 

    const onYes = () => {
        var data = {
            report_appid : appid,
            report_id : elements[0].value,
            report_name : elements[1].value,
            report_category : elements[2].value, 
            report_link : elements[3].value,
            report_proc : elements[4].value,
            report_status : (elements[5].value===1 ? 'A' : 'N'),
            report_accessstatus : 'E'
        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/editmenu/' + appid + '/' + menuid);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createmenu');
        }
        
    }

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
             <HeaderEnhancedx title='Menu' text='Menu Detail' />
             <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                   
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={2}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail isian menu. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>    
                )
            }
        </Container>
    )
}

export default MenuDetail;