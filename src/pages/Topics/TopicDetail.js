import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import LoadingBox2 from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import HeaderEnhancedx from '../../components/HeaderEnhancedx';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import ConfirmationBox from '../../components/ConfirmationBox';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const TopicDetail = props => {
    let history = useHistory();
    const {
        id,
        mode
    } = props.match.params
    const [ openload, setOpenLoad] = useState(false)
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const [ openconf, setOpenconf ] = useState(false)
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );

    useEffect(()=>{
        var elem = [];
        var options1 = [];
        var options2 = [];
        const apiUrl = URLLocation.getUrl() + '/getreports/1';
        setOpenLoad(true);
        fetch(apiUrl)
        .then((response) => response.json()
        .then((data) => 
            {
                data.map((obj)=>{
                    options1.push({
                        value :obj._id,
                        text : obj.reportcode + '   :   ' + obj.reportname 
                    })
                })
                const apiUrl2 = URLLocation.getUrl() + '/getroles/1';
                fetch(apiUrl2)
                .then((response) => response.json())
                .then((data) => {
                    data.map((obj)=>{
                        options2.push({
                            value :obj._id,
                            text : obj.rolename 
                        })
                    })
                    elem.push({
                        type:'text',
                        name:'topicname',
                        label:'Name',
                        placeholder:'Name',
                        value:'', 
                    })
        
                    elem.push({
                        type:'text',
                        name:'topiccode',
                        label:'Code',
                        placeholder:'Code',
                        value:'', 
                    })
                    elem.push({
                        type:'dropdown',
                        name:'Reports',
                        label:'Reports',
                        multiple:true,
                        value:0, 
                        options:options1
                    })     
                    elem.push({
                        type:'dropdown',
                        name:'Roles',
                        label:'Roles',
                        multiple:true,
                        value:0, 
                        options:options2
                    }) 
                    var options = [
                        {value : 100, text : 'Every one hours'},
                        {value : 4, text : 'Every four hours'},
                        {value : 5, text : 'Every five hours'},
                        {value : 8, text : 'Every eight hours'},
                        {value : 0, text : 'Daily'}, 
                        {value : 1, text : 'Weekly'}, 
                        {value : 2, text : 'Monthly'},
                        {value : 99, text : 'Every 1'},
                    ]
                    elem.push({
                        type:'dropdown',
                        name:'frequency',
                        label:'Frequency',
                        multiple:false,
                        value:0, 
                        options:options
                    })
                    elem.push({
                        type:'calendar',
                        name:'startdate',
                        label:'Start Run',
                        selectedDate:new Date(), 
                    })
                    elem.push({
                        type:'checkbox',
                        name:'is_active',
                        label:'Is Active',
                        value: 1, 
                    })
                    elem.push({
                        type:'label',
                        datatype:'DATETIME',
                        name:'lastrun',
                        label:'Last run (start date)',
                        value: new Date(),
                    })
                    elem.push({
                        type:'label',
                        datatype:'DATETIME',
                        name:'lastmodified',
                        label:'Last modified',
                        value: new Date(),
                    })
                    
                    setOpenLoad(false)
                    if (modestate==='edit') {
                        setAction('get');
                        setGetUrl(URLLocation.getUrl() + '/gettopicbyid/' + id,'')
                    }
                    else {
                        setModeState('add');
                        setElements(elem);
                    }
                }); 
            }
        ))
        .catch((err)=>{
            setOpenLoad(false);
            NotificationManager.error(err, 'Error', 3000);
        })
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        ) 
        {
            var idtemp = id;   
            var elem = [];
            var topicname = null;
            var topiccode = null;
            var reports = [];
            var emailroles = [];
            var emailscheduleselected = null;
            var is_active = null;
            var lastmodified = null;
            var emaillastrun = null;
            var startdate = null;

            if (action==='get') {
                topicname = stateGet.data[0].topicname; 
                topiccode = stateGet.data[0].topiccode;
                for (var i=0;i<stateGet.data[0].reports.length;i++) {
                    reports.push(stateGet.data[0].reports[i].value)
                }
                for (var i=0;i<stateGet.data[0].emailroles.length;i++) {
                    emailroles.push(stateGet.data[0].emailroles[i].value)
                }
                emailscheduleselected = stateGet.data[0].emailscheduleselected;
                startdate = stateGet.data[0].startdate;
                emaillastrun = stateGet.data[0].emaillastrun;
                is_active = stateGet.data[0].is_active;
                lastmodified = stateGet.data[0].lastmodified;
            }
            if (action==='put') {
                topicname = elements[0].value; 
                topiccode = elements[1].value;
                reports = elements[2].value;
                emailroles = elements[3].value;
                emailscheduleselected = elements[4].value;
                startdate = elements[5].selectedDate;
                is_active = elements[6].value;
                emaillastrun = elements[7].value;
                lastmodified = elements[8].value;
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
                topicname = elements[0].value; 
                topiccode = elements[1].value;
                reports = elements[2].value;
                emailroles = elements[3].value;
                emailscheduleselected = elements[4].value;
                startdate = elements[5].selectedDate;
                is_active = elements[6].value;
                emaillastrun = elements[7].value;
                lastmodified = elements[8].value;
            }
            
            var elem = [];
            var options1 = [];
            var options2 = [];
            const apiUrl = URLLocation.getUrl() + '/getreports/1';
            setOpenLoad(true);
            fetch(apiUrl)
            .then((response) => response.json()
            .then((data) => 
                {
                    data.map((obj)=>{
                        options1.push({
                            value :obj._id,
                            text : obj.reportcode + '   :   ' + obj.reportname 
                        })
                    })
                    const apiUrl2 = URLLocation.getUrl() + '/getroles/1';
                    fetch(apiUrl2)
                    .then((response) => response.json())
                    .then((data) => {
                        data.map((obj)=>{
                            options2.push({
                                value :obj._id,
                                text : obj.rolename 
                            })
                        })
                        elem.push({
                            type:'text',
                            name:'topicname',
                            label:'Name',
                            placeholder:'Name',
                            value:topicname, 
                        })
            
                        elem.push({
                            type:'text',
                            name:'topiccode',
                            label:'Code',
                            placeholder:'Code',
                            value:topiccode, 
                        })
                        elem.push({
                            type:'dropdown',
                            name:'Reports',
                            label:'Reports',
                            multiple:true,
                            value:reports, 
                            options:options1
                        })     
                        elem.push({
                            type:'dropdown',
                            name:'Roles',
                            label:'Roles',
                            multiple:true,
                            value:emailroles, 
                            options:options2
                        }) 
                        var options = [
                            {value : 100, text : 'Every one hours'},
                            {value : 4, text : 'Every four hours'},
                            {value : 5, text : 'Every five hours'},
                            {value : 8, text : 'Every eight hours'},
                            {value : 0, text : 'Daily'}, 
                            {value : 1, text : 'Weekly'}, 
                            {value : 2, text : 'Monthly'},
                            {value : 99, text : 'Every 1'},
                        ]
                        elem.push({
                            type:'dropdown',
                            name:'frequency',
                            label:'Frequency',
                            multiple:false,
                            value:emailscheduleselected, 
                            options:options
                        })
                        

                        elem.push({
                            type:'calendar',
                            name:'startdate',
                            label:'Start Run',
                            selectedDate:startdate, 
                        })

                        elem.push({
                            type:'checkbox',
                            name:'is_active',
                            label:'Is Active',
                            value: is_active, 
                        })

                        
                        elem.push({
                            type:'label',
                            datatype:'DATETIME',
                            name:'lastrun',
                            label:'Last run (start date)',
                            value: emaillastrun,
                        })

            
                        elem.push({
                            type:'label',
                            datatype:'DATETIME',
                            name:'lastmodified',
                            label:'Last modified',
                            value: lastmodified,
                        })
                        setOpenLoad(false)
                        setIDState(idtemp);
                        setElements(elem);
                        if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
                    }); 
                }
            ))
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onYes = () => {
        var data = {
            topicname : elements[0].value,
            topiccode : elements[1].value,
            reports : elements[2].value,
            emailroles : elements[3].value,
            msg1roles : [],
            emailscheduleselected : elements[4].value,
            startdate : elements[5].selectedDate,
            is_active : elements[6].value,
            emaillastrun : elements[7].value,
            lastmodified : elements[8].value
        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/edittopic/' + idstate);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createtopic');
        }
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    const onDateChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].selectedDate = value;
        setElements(elems);
    }

    const onRunEmailClick = () => {
        if (modestate==='edit') {
            const apiUrl = URLLocation.getUrl() + '/emailforcerun/' + idstate;
            setOpenLoad(true);
            fetch(apiUrl)
            .then((response) => response.json()
            .then((data) => {
                setOpenLoad(false);
                NotificationManager.success('Email run success', 'Success', 3000);
            }))
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
        }
        else 
            NotificationManager.error('Please save data first', 'Error', 3000);
    }

    const onCopyTopicClick = () => {
        setOpenconf(true)
    }

    const confirmCopyTopic = (inputText) => {
        setOpenconf(false);
        const apiUrl = URLLocation.getUrl() + '/copytopic';
        setOpenLoad(true);
        const data = { 
            sourcetopic_id : idstate,
            desttopic_code : inputText
        };
        fetch(apiUrl, {
            method: 'POST', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
        .then(response => response.json()
        .then(data => {
            setOpenLoad(false);
            if (data.result==='error')
                NotificationManager.error('Error when copying topic', 'Error', 3000);
            else
                NotificationManager.success('Copy topic success', 'Success', 3000);
        }))
        .catch((error) => {
            alert('Error:', error);
        });
    }

    const cancelCopyTopic = () => {
        setOpenconf(false)
    }

    return (
        <Container>
            <HeaderEnhancedx title='Topic' text='Topic detail' />
            <LoadingBox2 open={openload}/>
            <ConfirmationBox 
                message='Please fill new topic code'
                inputCaption={'New topic code'} 
                inputText=''
                open={openconf} 
                onYes={confirmCopyTopic} 
                onNo={cancelCopyTopic}
            />
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={1}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onDateChange={onDateChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail topik yang akan dikirim. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                    customButtonOne={'Run E-mail'}
                                    customButtonOneClick={onRunEmailClick}
                                    customButtonTwo={'Copy Topic'}
                                    customButtonTwoClick={onCopyTopicClick}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default TopicDetail;
