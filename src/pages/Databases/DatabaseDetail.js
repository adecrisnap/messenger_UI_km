import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import HeaderEnhancedx from '../../components/HeaderEnhancedx';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const DatabaseDetail = props => {
    let history = useHistory();
    const {
        id,
        mode
    } = props.match.params
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );


    useEffect(()=>{
        if (modestate==='edit') {
            setAction('get');
            setGetUrl(URLLocation.getUrl() + '/getserverbyid/' + id,'')
        }
        else {
            var elem = [];
            elem.push({
                type:'text',
                name:'servername',
                label:'Server Name',
                placeholder:'Server Name',
                value:'', 
            })

            var options = [{text:'MS SQL Server',value:0},{text:'MYSQL Server', value:1}]
            elem.push({
                type:'dropdown',
                name:'servertype',
                label:'Server Type',
                multiple:false,
                value:null, 
                options:options
            })

            elem.push({
                type:'text',
                name:'serveraddress',
                label:'Host Address',
                placeholder:'Host Address',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'dbname',
                label:'Database Name',
                placeholder:'Database Name',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'username',
                label:'User Name',
                placeholder:'User Name',
                value:'', 
            })

            elem.push({
                type:'password',
                name:'Password',
                label:'Password',
                placeholder:'Password',
                value:'', 
            })

            elem.push({
                type:'checkbox',
                name:'is_active',
                label:'Is Active',
                value: 1, 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'lastmodified',
                label:'Last Modified',
                value: new Date(),
            })

            setElements(elem);
        }
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        ) 
        {
            var idtemp = id;   
            var elem = [];
            var servername = null;
            var servertype = null;
            var serveraddress = null;
            var dbname = null;
            var username = null;
            var pwd = null;
            var is_active = null;
            var lastmodified = null;

            
            if (action==='get') {
                servername = stateGet.data[0].servername; 
                servertype = stateGet.data[0].servertype;
                serveraddress = stateGet.data[0].serveraddress; 
                dbname = stateGet.data[0].dbname; 
                username = stateGet.data[0].username;
                pwd = stateGet.data[0].pwd;  
                is_active = stateGet.data[0].is_active;
                lastmodified = stateGet.data[0].lastmodified;
            }
            if (action==='put') {
                servername = elements[0].value; 
                servertype = elements[1].value;
                serveraddress = elements[2].value; 
                dbname = elements[3].value; 
                username = elements[4].value;
                pwd = elements[5].value;  
                is_active = elements[6].value;
                lastmodified = elements[7].value;
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
                servername = elements[0].value; 
                servertype = elements[1].value;
                serveraddress = elements[2].value; 
                dbname = elements[3].value; 
                username = elements[4].value;
                pwd = elements[5].value;  
                is_active = elements[6].value;
                lastmodified = elements[7].value;
            }
            
            elem.push({
                type:'text',
                name:'servername',
                label:'Server Name',
                placeholder:'Server Name',
                value:servername, 
            })

            var options = [{text:'MS SQL Server',value:0},{text:'MYSQL Server', value:1}]
            elem.push({
                type:'dropdown',
                name:'servertype',
                label:'Server Type',
                multiple:false,
                value:servertype, 
                options:options
            })

            elem.push({
                type:'text',
                name:'serveraddress',
                label:'Host Address',
                placeholder:'Host Address',
                value:serveraddress, 
            })

            elem.push({
                type:'text',
                name:'dbname',
                label:'Database Name',
                placeholder:'Database Name',
                value:dbname, 
            })

            elem.push({
                type:'text',
                name:'username',
                label:'User Name',
                placeholder:'User Name',
                value:username, 
            })

            elem.push({
                type:'password',
                name:'Password',
                label:'Password',
                placeholder:'Password',
                value:pwd, 
            })

            elem.push({
                type:'checkbox',
                name:'is_active',
                label:'Is Active',
                value:is_active, 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'lastmodified',
                label:'Last Modified',
                value:lastmodified, 
            })

            setIDState(idtemp);
            setElements(elem);
            if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onYes = () => {
        var data = {
            servername : elements[0].value,
            servertype : elements[1].value,
            serveraddress : elements[2].value,
            dbname : elements[3].value,
            username : elements[4].value,
            pwd : elements[5].value,
            is_active : elements[6].value
        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/editserver/' + idstate);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createserver');
        }
        
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
            <HeaderEnhancedx title='Database' text='Database server' />
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={2}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail database di mana aplikasi messenger ini akan terhubung. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default DatabaseDetail;

/*

 ((action==='get' && stateGet.status!==200 && stateGet.status!==201 && stateGet.status!==304 && stateGet.status!==0) || 
                    (action==='post' && statePost.status!==200 && statePost.status!==201 && statePost.status!==0) ||
                    (action==='put' && statePut.status!==200 && statePut.status!==201 && statePut.status!==0) 
                    ) ?
                        (
                            <>
                                <Message negative>
                                    <Message.Header>Error</Message.Header>
                                    <p>{stateGet.errorMessage}</p>
                                </Message>
                                <Button onClick={onClose}>Back</Button>
                            </>
                        ) :

                    if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
            ) 
*/