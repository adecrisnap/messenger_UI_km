import React from 'react';
import { 
    Container,
    Tab,

} from 'semantic-ui-react';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import ApplicationDetail from './Applications/ApplicationDetail';
import AppMenus from './Applications/AppMenus';



const AppTab = props =>  {
    const panes = [
        { menuItem: { key: 'detail', content :'Detail' }, render: () => <Tab.Pane><ApplicationDetail mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
        { menuItem: { key: 'menus', content :'Menus' }, render: () => <Tab.Pane><AppMenus mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
      ]
      
    return (
        <Container>
            <HeaderEnhancedx title='Application' text='Application Detail' />
            <p/>
            <Tab menu={{ borderless: true, attached: false, tabular: false, fluid: true }} panes={panes} />
        </Container>
    );
    
}

export default AppTab;

//    { menuItem: { key: 'errorlogs', content :'Error Logs' }, render: () => <Tab.Pane><ErrorLogs/></Tab.Pane> },
    