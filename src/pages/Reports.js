import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const Reports = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getreports/-1');
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : '_id', columncaption : ' ', dataType : 'String', width : 5, link : 'reporttab/edit', visible : false})
            columns.push({columnname : 'reportname', columncaption : 'Report Name', dataType: 'String', width: 12});
            columns.push({columnname : 'reportcode', columncaption : 'Code', dataType: 'String', width: 3});
            columns.push({columnname : 'functionname', columncaption : 'Function Name', dataType: 'String', width: 6});
            columns.push({columnname : 'requestedby', columncaption : 'Requested By', dataType: 'String', width: 3});
            columns.push({columnname : 'is_active', columncaption : 'Is Active', dataType: 'Custom', width: 2, customvalues : [ {value : 0, text : 'Non active'}, {value: 1, text :'Active'}]});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 5});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhancedx title='Reports' text='All reports registered' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (  
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <GridTable
                                    datasource={dataSource} 
                                    columns={columns}
                                    linkinrowlevel={true}
                                    link={'/reporttab/add/0'}
                                    rowsperpage={10}
                                    sortedby={'reportname'}
                                    ascdesc={'ascending'}
                                    showaddnew={true}
                                    showaddnewonempty={false}
                                    emptycaption="You don't have any report"
                                    emptyimage={null}
                                    emptytitle="No report"
                                    emptybuttoncaption="Add new report"
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid> 
                )
            }
        </Container>
    )
}

export default Reports;

/*
const handleClick = () => {
        history.push('/reportdetail/add/0')
    }

 (stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No report"
                            caption="You don't have any report"
                            showButton={false}
                            buttonCaption = "Create new report"
                            onClick={handleClick} 
                        />
                    )
                    : 
                    (
                        */