import React from 'react';
import { 
    Container,
    Tab,

} from 'semantic-ui-react';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import Logs from './Home/Logs';
import ErrorLogs from './Home/ErrorLogs';



const Home = props =>  {
    const panes = [
        { menuItem: { key: 'logs', content :'Warning' }, render: () => <Tab.Pane><Logs/></Tab.Pane> },
        { menuItem: { key: 'errorlogs', content :'Error Logs' }, render: () => <Tab.Pane><ErrorLogs/></Tab.Pane> },
    ]
      
    return (
        <Container>
            <HeaderEnhancedx title='Home' text='Catatan kinerja pengiriman pesan' />
            <p/>
            <Tab menu={{ borderless: true, attached: false, tabular: false, fluid: true }} panes={panes} />
        </Container>
    );
    
}

export default Home;

//    { menuItem: { key: 'errorlogs', content :'Error Logs' }, render: () => <Tab.Pane><ErrorLogs/></Tab.Pane> },
    