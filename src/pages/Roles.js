import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
//import GridCard from '../components/GridCard';
import URLLocation from '../services/URLLocation';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const Roles = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getroles/-1');
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : '_id', columncaption : ' ', dataType : 'String', width : 5, link : 'roletab/edit', visible : false})
            columns.push({columnname : 'rolename', columncaption : 'Name', dataType: 'String', width: 5});
            columns.push({columnname : 'keyword', columncaption : 'Keyword', dataType: 'String', width: 10});
            columns.push({columnname : 'is_active', columncaption : 'Is Active', dataType: 'Custom', width: 3, customvalues : [ {value : 0, text : 'Non active'}, {value: 1, text :'Active'}]});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 5});
            setColumns(columns);
            
            /*
            var data = [];
            for (var i=0;i<stateGet.data.length;i++) {
                data.push({
                    header : stateGet.data[i].rolename,
                    subheader : stateGet.data[i].keyword,
                    description : stateGet.data[0].lastmodified
                })
            }
            */
            //alert(JSON.stringify(data))
            setDataSource(stateGet.data);
           //setDataSource(data);
            //setOriginalDs(data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhancedx title='Roles' text='All roles registered' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <GridTable
                                    datasource={dataSource} 
                                    columns={columns}
                                    rowsperpage={10}
                                    linkinrowlevel={true}
                                    link={'/roletab/add/0'}
                                    sortedby={'rolename'}
                                    ascdesc={'ascending'}
                                    showaddnew={true}
                                    showaddnewonempty={false}
                                    emptycaption="You don't have any role"
                                    emptyimage={null}
                                    emptytitle="No role"
                                    emptybuttoncaption="Add new role"
                                />
                                </Grid.Column>
                        </Grid.Row>
                    </Grid>
                )
            }
        </Container>
    )
}

export default Roles;

/*
(stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No role"
                            caption="You don't have any role"
                            showButton={false}
                            buttonCaption = "Create new role"
                            onClick={handleClick} 
                        />
                    )
                    : 
                    (
 

<GridCard
                                    datasource={dataSource} 
                                    columnsnumber={3}
                                    recordsperpage={10}
                                    linkinrowlevel={true}
                                    link={'/roletab/add/0'}
                                    
                                    onSearching={onSearching}
                                    sortedby={'rolename'}
                                    ascdesc={'ascending'}
                                    showaddnew={true}
                                    showaddnewonempty={false}
                                    emptycaption="You don't have any role"
                                    emptyimage={null}
                                    emptytitle="No role"
                                    emptybuttoncaption="Add new role"
                                />
                            </Grid.Column>

*/