import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const Databases = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getservers/-1');
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : '_id', columncaption : '', dataType : 'String', width : 5, link : 'databasedetail/edit', visible:false})
            columns.push({columnname : 'servername', columncaption : 'Name', dataType: 'String', width: 8});
            columns.push({columnname : 'servertype', columncaption : 'Server Type', dataType: 'Custom', width: 5, customvalues : [ {value : 0, text : 'MS SQL Server'}, {value: 1, text :'MySQL Server'}]});
            columns.push({columnname : 'dbname', columncaption : 'Database Name', dataType: 'String', width: 8});
            columns.push({columnname : 'is_active', columncaption : 'Is Active', dataType: 'Custom', width: 5, customvalues : [ {value : 0, text : 'Non active'}, {value: 1, text :'Active'}]});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 8});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhancedx title='Databases' text='Database servers' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        linkinrowlevel={true}
                        link={'/databasedetail/add/0'}
                        sortedby={'servername'}
                        ascdesc={'ascending'}
                        showaddnew={true}
                        showaddnewonempty={false}
                        emptycaption="You don't have any database"
                        emptyimage={null}
                        emptytitle="No database"
                        emptybuttoncaption="Add new database"
                    />
                )
            }
        </Container>
    )
}

export default Databases;

/*
 (stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No database"
                            caption="You don't have any database"
                            showButton={false}
                            buttonCaption = "Create new database"
                            onClick={handleClick} 
                        />
                    )
                    : 
                    (

                        
    const handleClick = () => {
        history.push('/databasedetail/add/0')
    }

*/