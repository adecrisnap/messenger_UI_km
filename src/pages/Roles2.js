import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import GridCard from '../components/GridCard'
import URLLocation from '../services/URLLocation';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const Roles2 = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getroles/-1');
    },[])

    useEffect(()=>{
        
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            
            var columns = [];
            columns.push({columnname : '_id', columncaption : ' ', dataType : 'String', width : 5, link : 'roletab/edit', visible : false})
            columns.push({columnname : 'rolename', columncaption : 'Name', dataType: 'String', width: 5});
            columns.push({columnname : 'keyword', columncaption : 'Keyword', dataType: 'String', width: 10});
            columns.push({columnname : 'is_active', columncaption : 'Is Active', dataType: 'Custom', width: 3, customvalues : [ {value : 0, text : 'Non active'}, {value: 1, text :'Active'}]});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 5});
            var data = [];
            if (stateGet.data!==null) {
            stateGet.data.map((obj)=>{
                data.push({
                    header : '',
                    subheader : '',
                    content : obj.rolename,
                    footer : obj.lastmodified
                })
            });
        }
            //alert(JSON.stringify(data));
            setColumns(columns);
            setDataSource(data);
        }
        else {
            alert(stateGet.status)
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhancedx title='Roles' text='All roles registered' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <GridCard
                                    datasource={dataSource} 
                                    columnsnumber={4}
                                    rowsperpage={2}
                                    internalclick={true}
                                    sortedby={'rolename'}
                                    ascdesc={'ascending'}
                                    link={'/roletab/add/0'}
                                    showaddnew={true}
                                    showaddnewonempty={false}
                                    emptycaption="You don't have any role"
                                    emptyimage={null}
                                    emptytitle="No role"
                                    emptybuttoncaption="Add new role"
                                    optionssortby="rolename"
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                )
            }
        </Container>
    )
}

export default Roles2;

/*
(stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No role"
                            caption="You don't have any role"
                            showButton={false}
                            buttonCaption = "Create new role"
                            onClick={handleClick} 
                        />
                    )
                    : 
                    (


*/