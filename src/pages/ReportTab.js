import React from 'react';
import { 
    Container,
    Tab,

} from 'semantic-ui-react';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import ReportDetail from './Reports/ReportDetail';
import Columns from './Reports/Columns';



const ReportTab = props =>  {
    const panes = [
        { menuItem: { key: 'detail', content :'Detail' }, render: () => <Tab.Pane><ReportDetail mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
        { menuItem: { key: 'columns', content :'Columns' }, render: () => <Tab.Pane><Columns mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
      ]
      
    return (
        <Container>
            <HeaderEnhancedx title='Report' text='Detail Report' />
            <p/>
            <Tab menu={{ borderless: true, attached: false, tabular: false, fluid: true }} panes={panes} />
        </Container>
    );
    
}

export default ReportTab;

//    { menuItem: { key: 'errorlogs', content :'Error Logs' }, render: () => <Tab.Pane><ErrorLogs/></Tab.Pane> },
    