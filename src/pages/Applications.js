import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const Applications = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getapps/-1');
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];

            columns.push({columnname : '_id', columncaption : ' ', dataType : 'String', width : 5, link : 'applicationdetail/edit', visible : false})
            columns.push({columnname : 'appname', columncaption : 'Application Name', dataType: 'String', width: 12});
            columns.push({columnname : 'appcode', columncaption : 'Application Code', dataType: 'String', width: 5});
            columns.push({columnname : 'appdesc', columncaption : 'Description', dataType: 'String', width: 12});
            columns.push({columnname : 'is_active', columncaption : 'Is Active', dataType: 'Custom', width: 3, customvalues : [ {value : 0, text : 'Non active'}, {value: 1, text :'Active'}]});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 5});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhancedx title='Applications' text='All applications registered' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <GridTable
                                    datasource={dataSource} 
                                    columns={columns}
                                    rowsperpage={10}
                                    sortedby={'servername'}
                                    linkinrowlevel={true}
                                    link={'/applicationdetail/add/0'}
                                    
                                    ascdesc={'ascending'}
                                    showaddnew={true}
                                    showaddnewonempty={false}
                                    emptycaption="You don't have any application"
                                    emptyimage={null}
                                    emptytitle="No application"
                                    emptybuttoncaption="Add new application"
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                )
            }
        </Container>
    )
}

export default Applications;
