import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import GridTable from '../../components/GridTable'
import URLLocation from '../../services/URLLocation';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const ErrorLogs = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/geterror/');
    },[])

    useEffect(()=>{
        //setGetUrl(URLLocation.getUrl() + '/geterror/')
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : 'lastmodified', columncaption : 'Date', dataType: 'DateTime', width: 2});
            columns.push({columnname : 'step', columncaption : 'Step', dataType: 'String', width: 2});
            columns.push({columnname : 'error_desc', columncaption : 'Error Message', dataType: 'String', width: 35 });
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    /*
        useEffect(()=>{
            if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
                var columns = [];
                columns.push({columnname : 'lastmodified', columncaption : 'Date', dataType: 'DateTime', width: 5});
                columns.push({columnname : 'step', columncaption : 'Step', dataType: 'String', width: 8});
                columns.push({columnname : 'error_desc', columncaption : 'Error Message', dataType: 'String', width: 10});
                setColumns(columns);
                setDataSource(stateGet.data);
                setOriginalDs(stateGet.data);
            }
        },[stateGet.status])
    */

    return (
        <Container>
            <NotificationContainer/>
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <GridTable
                                    datasource={dataSource} 
                                    columns={columns}
                                    rowsperpage={10}
                                    sortedby={'lastmodified'}
                                    ascdesc={'descending'}
                                    showaddnewonempty={false}
                                    emptycaption="You don't have any log"
                                    emptyimage={null}
                                    emptytitle="No Log"
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                )
            }
        </Container>
    )
}

export default ErrorLogs;

/*


 (stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No Log"
                            caption="You don't have any log"
                            showButton={false}
                            //buttonCaption = "Create New Client"
                            //onClick={onButtonClick} 
                        />
                    )
                    : 
                    (

                        */