import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import ConfirmationBox from '../../components/ConfirmationBox';
import useGetData from '../../services/useGetData';
import GridTable from '../../components/GridTable'
import URLLocation from '../../services/URLLocation';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import DeleteDataToServer from '../../services/useDeleteData';
import { useHistory } from "react-router-dom";


const Columns = props => {
    let history = useHistory();
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const [idtodelete,setIdtodelete] = useState(null)
    const [openconf, setOpenconf] = useState(false)
    const  { stateGet,setGetUrl, setRefresh } = useGetData(
        null,''
    );
    const  { stateDelete, setDeleteUrl } = DeleteDataToServer(
        null,''
    );
    const {
        id,
        mode
    } = props
    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getcolumns/' + id);
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : '_id', columncaption : '', dataType : 'Button',  width : 2, link : 'columndetail/edit/' + id, action : 'edit', buttonCaption : 'Edit', visible : true})
            columns.push({columnname : '_id', columncaption : '', dataType : 'Button',  width : 3, action : 'delete', buttonCaption : 'Delete', visible : true})
            columns.push({columnname : 'columnname', columncaption : 'Column Name', dataType: 'String', width: 12});
            columns.push({columnname : 'displayname', columncaption : 'Display Name', dataType: 'String', width: 12});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 5});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    useEffect(()=>{
        if (stateDelete.status===200 || stateDelete.status===201) {
           setRefresh(true)
        }
        else {
            var msg = '';
            msg = stateDelete.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateDelete.randomstatus])

    const buttonClick = (value,action) =>{
        if (action==='delete') {
            setIdtodelete(value);
            setOpenconf(true)
        }
        else {
            history.push('/columndetail/edit/' + id + '/' + value)
        }
    }

    const handleCancel = () => {
        setOpenconf(false)
    }

    const handleConfirm = (inputText) => {
        setOpenconf(false)
        setDeleteUrl(URLLocation.getUrl() + '/deletecolumn/' + idtodelete);
    }

    return (
        <Container>
            <ConfirmationBox 
                message='Are you sure you want to delete ?' 
                open={openconf} 
                onYes={handleConfirm} 
                onNo={handleCancel}
            />
            <NotificationContainer/>
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        linkinrowlevel={false}
                        link={'/columndetail/add/' + id + '/0'}
                        
                        sortedby={'columnname'}
                        ascdesc={'ascending'}
                        
                        internalclick={false}
                        buttonClick={buttonClick}
                        
                        
                        showaddnew={true}
                        showaddnewonempty={false}
                        emptycaption="You don't have any column"
                        emptyimage={null}
                        emptytitle="No column"
                        emptybuttoncaption="Add new column"

                    />
                )
            }
        </Container>
    )
}

export default Columns;
