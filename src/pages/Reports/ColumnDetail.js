import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import HeaderEnhancedx from '../../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const ColumnDetail = props => {
    let history = useHistory();
    const {
        idreport,
        id,
        mode
    } = props.match.params
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );
   
    
    useEffect(()=>{      
        if (modestate==='edit') {
            setAction('get');
            setGetUrl(URLLocation.getUrl() + '/getcolumnbyid/' + id,'')
        }
        else {
            var elem = [];
            elem.push({
                type:'text',
                name:'columnname',
                label:'Column Name',
                placeholder:'Column Name',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'displayname',
                label:'Display Name',
                placeholder:'Display Name',
                value:'', 
            })

            var textAlignments = [ { value : 0, text : 'Left'}, { value : 1, text : 'Right'}]
            elem.push({
                type:'dropdown',
                name:'textalignment',
                label:'Select Alignment',
                multiple:false,
                value:null, 
                options:textAlignments
            })

            var formatcolumns = [ { value : 0, text : 'Text'}, { value : 1, text : 'Number with 2 decimals'}, { value : 2, text : 'Number without decimal'}, { value : 3, text : 'Short date'}, { value : 4, text : 'Long date'}]
            elem.push({
                type:'dropdown',
                name:'formatcolumn',
                label:'Select Format',
                multiple:false,
                value:null, 
                options:formatcolumns
            })

            var highlight = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnhighlight',
                label:'Select Highlight',
                multiple:false,
                value:null, 
                options:highlight
            })

            var conditionalx = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnconditional',
                label:'Select Conditional',
                multiple:false,
                value:null, 
                options:conditionalx
            })

            //target1
            var istarget1 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columntarget',
                label:'Is Target 1',
                multiple:false,
                value:null, 
                options:istarget1
            })

            var operand1 = [ { value : 0, text : '<'}, { value : 1, text : '>'}]
            elem.push({
                type:'dropdown',
                name:'columnoperand1',
                label:'Operand 1',
                multiple:false,
                value:null, 
                options:operand1
            })

            var isactual1 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnactual',
                label:'Is Actual 1',
                multiple:false,
                value:null, 
                options:isactual1
            })

            var coloractual1 = [ { value : 0, text : 'Red'}, { value : 1, text : 'Green'}]
            elem.push({
                type:'dropdown',
                name:'columncoloractual',
                label:'Color 1',
                multiple:false,
                value:null, 
                options:coloractual1
            })

            //target2
            var istarget2 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columntarget2',
                label:'Is Target 2',
                multiple:false,
                value:null, 
                options:istarget2
            })

            var operand2 = [ { value : 0, text : '<'}, { value : 1, text : '>'}]
            elem.push({
                type:'dropdown',
                name:'columnoperand2',
                label:'Operand 2',
                multiple:false,
                value:null, 
                options:operand2
            })

            var isactual2 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnactual2',
                label:'Is Actual 2',
                multiple:false,
                value:null, 
                options:isactual2
            })

            var coloractual2 = [ { value : 0, text : 'Red'}, { value : 1, text : 'Green'}]
            elem.push({
                type:'dropdown',
                name:'columncoloractual2',
                label:'Color 2',
                multiple:false,
                value:null, 
                options:coloractual2
            })

            //target3
            var istarget3 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columntarget3',
                label:'Is Target 3',
                multiple:false,
                value:null, 
                options:istarget3
            })

            var operand3 = [ { value : 0, text : '<'}, { value : 1, text : '>'}]
            elem.push({
                type:'dropdown',
                name:'columnoperand3',
                label:'Operand 3',
                multiple:false,
                value:null, 
                options:operand3
            })

            var isactual3 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnactual3',
                label:'Is Actual 3',
                multiple:false,
                value:null, 
                options:isactual3
            })

            var coloractual3 = [ { value : 0, text : 'Red'}, { value : 1, text : 'Green'}]
            elem.push({
                type:'dropdown',
                name:'columncoloractual3',
                label:'Color 3',
                multiple:false,
                value:null, 
                options:coloractual3
            })

            setElements(elem);
        }
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
            ) 
        {
            var idtemp = id;   
            var elem = [];
            
            var columnname = null;
            var displayname = null;
            var textalignment = null;
            var formatcolumn = null;
            var columnhighlight = null;
            var columnconditional = null;
            
            var columntarget1 =null;
            var columnoperand1 = null;
            var columnactual1 = null;
            var columncoloractual1 = null
            
            var columntarget2 =null;
            var columnoperand2 = null;
            var columnactual2 = null;
            var columncoloractual2 = null
            
            var columntarget3 =null;
            var columnoperand3 = null;
            var columnactual3 = null;
            var columncoloractual3 = null
            
            if (action==='get') {
                columnname = stateGet.data[0].columnname;
                displayname = stateGet.data[0].displayname;
                textalignment = stateGet.data[0].textalignment;
                formatcolumn = stateGet.data[0].formatcolumn;
                columnhighlight = stateGet.data[0].columnhighlight;
                columnconditional = stateGet.data[0].columnconditional;
                
                columntarget1 = stateGet.data[0].columntarget;
                columnoperand1 = stateGet.data[0].columnoperand1;
                columnactual1 = stateGet.data[0].columnactual;
                columncoloractual1 = stateGet.data[0].columncoloractual;
                
                columntarget2 = stateGet.data[0].columntarget2;
                columnoperand2 = stateGet.data[0].columnoperand2;
                columnactual2 = stateGet.data[0].columnactual2;
                columncoloractual2 = stateGet.data[0].columncoloractual2;
                
                columntarget3 = stateGet.data[0].columntarget3;
                columnoperand3 = stateGet.data[0].columnoperand3;
                columnactual3 = stateGet.data[0].columnactual3;
                columncoloractual3 = stateGet.data[0].columncoloractual3;
            }
            if (action==='put') {
                columnname = elements[0].value;
                displayname = elements[1].value;
                textalignment = elements[2].value;
                formatcolumn = elements[3].value;
                columnhighlight = elements[4].value;
                columnconditional = elements[5].value;
                
                columntarget1 = elements[6].value;
                columnoperand1 = elements[7].value;
                columnactual1 = elements[8].value;
                columncoloractual1 = elements[9].value;
                
                columntarget2 = elements[10].value;
                columnoperand2 = elements[11].value;
                columnactual2 = elements[12].value;
                columncoloractual2 = elements[13].value;
                
                columntarget3 = elements[14].value;
                columnoperand3 = elements[15].value;
                columnactual3 = elements[16].value;
                columncoloractual3 = elements[17].value;
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
                columnname = elements[0].value;
                displayname = elements[1].value;
                textalignment = elements[2].value;
                formatcolumn = elements[3].value;
                columnhighlight = elements[4].value;
                columnconditional = elements[5].value;
                
                columntarget1 = elements[6].value;
                columnoperand1 = elements[7].value;
                columnactual1 = elements[8].value;
                columncoloractual1 = elements[9].value;
                
                columntarget2 = elements[10].value;
                columnoperand2 = elements[11].value;
                columnactual2 = elements[12].value;
                columncoloractual2 = elements[13].value;
                
                columntarget3 = elements[14].value;
                columnoperand3 = elements[15].value;
                columnactual3 = elements[16].value;
                columncoloractual3 = elements[17].value;
            }

            elem.push({
                type:'text',
                name:'columnname',
                label:'Column Name',
                placeholder:'Column Name',
                value:columnname, 
            })

            elem.push({
                type:'text',
                name:'displayname',
                label:'Display Name',
                placeholder:'Display Name',
                value:displayname, 
            })

            var textAlignments = [ { value : 0, text : 'Left'}, { value : 1, text : 'Right'}]
            elem.push({
                type:'dropdown',
                name:'textalignment',
                label:'Select Alignment',
                multiple:false,
                value:textalignment, 
                options:textAlignments
            })

            var formatcolumns = [ { value : 0, text : 'Text'}, { value : 1, text : 'Number with 2 decimals'}, { value : 2, text : 'Number without decimal'}, { value : 3, text : 'Short date'}, { value : 4, text : 'Long date'}]
            elem.push({
                type:'dropdown',
                name:'formatcolumn',
                label:'Select Format',
                multiple:false,
                value:formatcolumn, 
                options:formatcolumns
            })

            var highlight = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnhighlight',
                label:'Select Highlight',
                multiple:false,
                value:columnhighlight, 
                options:highlight
            })

            var conditionalx = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnconditional',
                label:'Select Conditional',
                multiple:false,
                value:columnconditional, 
                options:conditionalx
            })

            //target1
            var istarget1 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columntarget',
                label:'Is Target 1',
                multiple:false,
                value:columntarget1, 
                options:istarget1
            })

            var operand1 = [ { value : 0, text : '<'}, { value : 1, text : '>'}]
            elem.push({
                type:'dropdown',
                name:'columnoperand1',
                label:'Operand 1',
                multiple:false,
                value:columnoperand1, 
                options:operand1
            })

            var isactual1 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnactual',
                label:'Is Actual 1',
                multiple:false,
                value:columnactual1, 
                options:isactual1
            })

            var coloractual1 = [ { value : 0, text : 'Red'}, { value : 1, text : 'Green'}]
            elem.push({
                type:'dropdown',
                name:'columncoloractual',
                label:'Color 1',
                multiple:false,
                value:columncoloractual1, 
                options:coloractual1
            })

            //target2
            var istarget2 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columntarget2',
                label:'Is Target 2',
                multiple:false,
                value:columntarget2, 
                options:istarget2
            })

            var operand2 = [ { value : 0, text : '<'}, { value : 1, text : '>'}]
            elem.push({
                type:'dropdown',
                name:'columnoperand2',
                label:'Operand 2',
                multiple:false,
                value:columnoperand2, 
                options:operand2
            })

            var isactual2 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnactual2',
                label:'Is Actual 2',
                multiple:false,
                value:columnactual2, 
                options:isactual2
            })

            var coloractual2 = [ { value : 0, text : 'Red'}, { value : 1, text : 'Green'}]
            elem.push({
                type:'dropdown',
                name:'columncoloractual2',
                label:'Color 2',
                multiple:false,
                value:columncoloractual2, 
                options:coloractual2
            })

            //target3
            var istarget3 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columntarget3',
                label:'Is Target 3',
                multiple:false,
                value:columntarget3, 
                options:istarget3
            })

            var operand3 = [ { value : 0, text : '<'}, { value : 1, text : '>'}]
            elem.push({
                type:'dropdown',
                name:'columnoperand3',
                label:'Operand 3',
                multiple:false,
                value:columnoperand3, 
                options:operand3
            })

            var isactual3 = [ { value : 0, text : 'No'}, { value : 1, text : 'Yes'}]
            elem.push({
                type:'dropdown',
                name:'columnactual3',
                label:'Is Actual 3',
                multiple:false,
                value:columnactual3, 
                options:isactual3
            })

            var coloractual3 = [ { value : 0, text : 'Red'}, { value : 1, text : 'Green'}]
            elem.push({
                type:'dropdown',
                name:'columncoloractual3',
                label:'Color 3',
                multiple:false,
                value:columncoloractual3, 
                options:coloractual3
            })

        
            setIDState(idtemp);
            setElements(elem);
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onClose = () => {
        history.goBack();
    } 

    const onYes = () => {
        var data = {
            id_report : idreport,
            columnname : elements[0].value,
            displayname : elements[1].value,
            textalignment : elements[2].value,
            formatcolumn : elements[3].value,
            columnhighlight : elements[4].value,
            columnconditional : elements[5].value,
            
            columntarget : elements[6].value,
            columnoperand1 : elements[7].value,
            columnactual : elements[8].value,
            columncoloractual : elements[9].value,
            
            columntarget2 : elements[10].value,
            columnoperand2 : elements[11].value,
            columnactual2 : elements[12].value,
            columncoloractual2 : elements[13].value,
            
            columntarget3 : elements[14].value,
            columnoperand3 : elements[15].value,
            columnactual3 : elements[16].value,
            columncoloractual3 : elements[17].value
        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/editcolumn/' + idstate);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createcolumn');
        }
        
    }

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
            <HeaderEnhancedx title='Column' text='Column detail' />
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                   
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={3}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail kolom. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>    
                )
            }
        </Container>
    )
}

export default ColumnDetail;