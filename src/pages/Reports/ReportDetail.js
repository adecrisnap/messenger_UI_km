import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import LoadingBox2 from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import ConfirmationBox from '../../components/ConfirmationBox';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const ReportDetail = props => {
    let history = useHistory();
    const {
        id,
        mode
    } = props
    const [ openload, setOpenLoad] = useState(false)
    //const [ optionsServers, setOptionsServers] = useState([]);
    const [ openconf, setOpenconf ] = useState(false)
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );

    useEffect(()=>{
        var elem = [];
        var options1 = [];
        const apiUrl = URLLocation.getUrl() + '/getservers/1';
        setOpenLoad(true);
        fetch(apiUrl)
        .then((response) => response.json()
        .then((data) => 
            {
                data.map((obj)=>{
                    options1.push({
                        value :obj._id,
                        text : obj.servername 
                    })
                })
                elem.push({
                    type:'text',
                    name:'reportcode',
                    label:'Code',
                    placeholder:'Code',
                    value:'', 
                })
                elem.push({
                    type:'text',
                    name:'reportname',
                    label:'Name',
                    placeholder:'Name',
                    value:'', 
                })
                elem.push({
                    type:'dropdown',
                    name:'serveraddress',
                    label:'Server',
                    multiple:false,
                    value:0, 
                    options:options1
                })
                elem.push({
                    type:'text',
                    name:'functionname',
                    label:'Function Name',
                    placeholder:'Function Name',
                    value:'', 
                })    
                elem.push({
                    type:'text',
                    name:'plantcolumn',
                    label:'Plant Column Name',
                    placeholder:'Plant Column Name',
                    value:'', 
                })
                elem.push({
                    type:'text',
                    name:'isfooter',
                    label:'Footer Column Name',
                    placeholder:'Footer Column Name',
                    value:'', 
                })  
                elem.push({
                    type:'memo',
                    name:'reportheader',
                    label:'Header',
                    placeholder:'Header',
                    value:'', 
                }) 
                elem.push({
                    type:'memo',
                    name:'reportfooter',
                    label:'Footer',
                    placeholder:'Footer',
                    value:'', 
                }) 
                elem.push({
                    type:'text',
                    name:'requestedby',
                    label:'Requested By',
                    placeholder:'Requested By',
                    value:'', 
                })  
                elem.push({
                    type:'checkbox',
                    name:'isperorganization',
                    label:'Split by organization ?',
                    value: 0, 
                })
                elem.push({
                    type:'checkbox',
                    name:'isplantvisible',
                    label:'Plant not visible ?',
                    value: 0, 
                })
                elem.push({
                    type:'checkbox',
                    name:'additionalsplitcolumn',
                    label:'Split by additional column ?',
                    value: 0, 
                })
                elem.push({
                    type:'text',
                    name:'additionalcolumn',
                    label:'Additional Column Name For Splitting',
                    placeholder:'Additional Column Name For Splitting',
                    value:'', 
                })  
                elem.push({
                    type:'checkbox',
                    name:'is_active',
                    label:'Is Active',
                    value: 1, 
                })
                elem.push({
                    type:'label',
                    datatype:'DATETIME',
                    name:'lastmodified',
                    label:'Last modified',
                    value: new Date(),
                })
                
                setOpenLoad(false)
                //setOptionsServers(options1);
                if (modestate==='edit') {
                    setAction('get');
                    setGetUrl(URLLocation.getUrl() + '/getreportbyid/' + id,'')
                }
                else {
                    setModeState('add');
                    setElements(elem);
                }
            }
        ))
        .catch((err)=>{
            setOpenLoad(false);
            NotificationManager.error(err, 'Error', 3000);
        })
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        ) 
        {
            var idtemp = id; 
            var reportcode = null
            var reportname = null
            var serveraddress = null
            var functionname = null
            var plantcolumn = null
            var isfooter = null
            var reportheader = null
            var reportfooter = null
            var requestedby = null
            var isperorganization = null
            var isplantvisible = null
            var is_active = null    
            var lastmodified = null
            var additionalsplitcolumn = null
            var additionalcolumn = null

            if (action==='get') {
                reportcode = stateGet.data[0].reportcode
                reportname = stateGet.data[0].reportname
                serveraddress = stateGet.data[0].serveraddress
                functionname = stateGet.data[0].functionname
                plantcolumn = stateGet.data[0].plantcolumn
                isfooter = stateGet.data[0].isfooter
                reportheader = stateGet.data[0].reportheader
                reportfooter = stateGet.data[0].reportfooter
                requestedby = stateGet.data[0].requestedby
                isperorganization = stateGet.data[0].isperorganization
                isplantvisible = stateGet.data[0].isplantvisible
                additionalsplitcolumn = stateGet.data[0].additionalsplitcolumn
                additionalcolumn = stateGet.data[0].additionalcolumn
                is_active = stateGet.data[0].is_active    
                lastmodified = stateGet.data[0].lastmodified
            }
            if (action==='put') {
                reportcode = elements[0].value
                reportname = elements[1].value
                serveraddress = elements[2].value
                functionname = elements[3].value
                plantcolumn = elements[4].value
                isfooter = elements[5].value
                reportheader = elements[6].value
                reportfooter = elements[7].value
                requestedby = elements[8].value
                isperorganization = elements[9].value
                isplantvisible = elements[10].value
                additionalsplitcolumn = elements[11].value
                additionalcolumn = elements[12].value
                is_active = elements[13].value
                lastmodified = elements[14].value    
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
                reportcode = elements[0].value
                reportname = elements[1].value
                serveraddress = elements[2].value
                functionname = elements[3].value
                plantcolumn = elements[4].value
                isfooter = elements[5].value
                reportheader = elements[6].value
                reportfooter = elements[7].value
                requestedby = elements[8].value
                isperorganization = elements[9].value
                isplantvisible = elements[10].value
                additionalsplitcolumn = elements[11].value
                additionalcolumn = elements[12].value
                is_active = elements[13].value
                lastmodified = elements[14].value
            }
            
            var elem = [];
            var options1 = [];
            const apiUrl = URLLocation.getUrl() + '/getservers/1';
            setOpenLoad(true);
            fetch(apiUrl)
            .then((response) => response.json()
            .then((data) => 
                {
                    data.map((obj)=>{
                        options1.push({
                            value :obj._id,
                            text : obj.servername 
                        })
                    })
                    elem.push({
                        type:'text',
                        name:'reportcode',
                        label:'Code',
                        placeholder:'Code',
                        value:reportcode, 
                    })
                    elem.push({
                        type:'text',
                        name:'reportname',
                        label:'Name',
                        placeholder:'Name',
                        value:reportname, 
                    })
                    elem.push({
                        type:'dropdown',
                        name:'serveraddress',
                        label:'Server',
                        multiple:false,
                        value:serveraddress, 
                        options:options1
                    })
                    elem.push({
                        type:'text',
                        name:'functionname',
                        label:'Function Name',
                        placeholder:'Function Name',
                        value:functionname, 
                    })    
                    elem.push({
                        type:'text',
                        name:'plantcolumn',
                        label:'Plant Column Name',
                        placeholder:'Plant Column Name',
                        value:plantcolumn, 
                    })
                    elem.push({
                        type:'text',
                        name:'isfooter',
                        label:'Footer Column Name',
                        placeholder:'Footer Column Name',
                        value:isfooter, 
                    })  
                    elem.push({
                        type:'memo',
                        name:'reportheader',
                        label:'Header',
                        placeholder:'Header',
                        value:reportheader, 
                    }) 
                    elem.push({
                        type:'memo',
                        name:'reportfooter',
                        label:'Footer',
                        placeholder:'Footer',
                        value:reportfooter, 
                    }) 
                    elem.push({
                        type:'text',
                        name:'requestedby',
                        label:'Requested By',
                        placeholder:'Requested By',
                        value:requestedby, 
                    })  
                    elem.push({
                        type:'checkbox',
                        name:'isperorganization',
                        label:'Split by organization ?',
                        value:isperorganization, 
                    })
                    elem.push({
                        type:'checkbox',
                        name:'isplantvisible',
                        label:'Plant not visible ?',
                        value: isplantvisible, 
                    })
                    elem.push({
                        type:'checkbox',
                        name:'additionalsplitcolumn',
                        label:'Split by additional column ?',
                        value: additionalsplitcolumn, 
                    })
                    elem.push({
                        type:'text',
                        name:'additionalcolumn',
                        label:'Additional Column Name For Splitting',
                        placeholder:'Additional Column Name For Splitting',
                        value:additionalcolumn, 
                    })  
                    elem.push({
                        type:'checkbox',
                        name:'is_active',
                        label:'Is Active',
                        value: is_active, 
                    })
                    elem.push({
                        type:'label',
                        datatype:'DATETIME',
                        name:'lastmodified',
                        label:'Last modified',
                        value: lastmodified,
                    })
                    setOpenLoad(false)
                    //setOptionsServers(options1);
                    setIDState(idtemp);
                    setElements(elem);
                    if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
                }
            ))
            .catch((err)=>{
                setOpenLoad(false);
                NotificationManager.error(err, 'Error', 3000);
            })
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onYes = () => {
        var data = {
            reportcode : elements[0].value,
            reportname : elements[1].value,
            serveraddress : elements[2].value,
            functionname : elements[3].value,
            plantcolumn : elements[4].value,
            isfooter : elements[5].value,
            reportheader : elements[6].value,
            reportfooter : elements[7].value,
            requestedby : elements[8].value,
            isperorganization : elements[9].value,
            isplantvisible : elements[10].value,
            additionalsplitcolumn : elements[11].value,
            additionalcolumn : elements[12].value,
            is_active : elements[13].value,    
        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/editreport/' + idstate);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createreport');
        }
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    const onDateChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].selectedDate = value;
        setElements(elems);
    }

    const onCopyReportClick = () => {
        setOpenconf(true)
    }

    const confirmCopyReport = (inputText) => {
        setOpenconf(false);
        const apiUrl = URLLocation.getUrl() + '/copyreport';
        setOpenLoad(true);
        const data = { 
            sourcereport_id : idstate,
            destreport_code : inputText
        };
        fetch(apiUrl, {
            method: 'POST', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
        .then(response => response.json()
        .then(data => {
            setOpenLoad(false);
            if (data.result==='error')
                NotificationManager.error('Error when copying report', 'Error', 3000);
            else
                NotificationManager.success('Copy report success', 'Success', 3000);
        }))
        .catch((error) => {
            alert('Error:', error);
        });
    }

    const cancelCopyReport = () => {
        setOpenconf(false)
    }

    return (
        <Container>
            <ConfirmationBox 
                message='Please fill new report code'
                inputCaption={'New report code'} 
                inputText=''
                open={openconf} 
                onYes={confirmCopyReport} 
                onNo={cancelCopyReport}
            />
            <LoadingBox2 open={openload}/>
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={2}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onDropdownChange={onDropdownChange}
                                    onDateChange={onDateChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail report yang akan diproses. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                    customButtonOne={'Copy Report'}
                                    customButtonOneClick={onCopyReportClick}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default ReportDetail;
