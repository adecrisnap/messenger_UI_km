import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../../components/LoadingBox';
import LoadingBox2 from '../../components/LoadingBox';
import useGetData from '../../services/useGetData';
import usePostData from '../../services/usePostData';
import usePutData from '../../services/usePutData';
import SimpleForm from '../../components/SimpleForm'
import URLLocation from '../../services/URLLocation';
import HeaderEnhancedx from '../../components/HeaderEnhancedx';
import { useHistory } from "react-router-dom";
import Helper from '../../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


const MailServerDetail = props => {
    let history = useHistory();
    const {
        id,
        mode
    } = props.match.params
    const [ openload, setOpenLoad] = useState(false)
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );

    useEffect(()=>{
        if (modestate==='edit') {
            setAction('get');
            setGetUrl(URLLocation.getUrl() + '/getmailserverbyid/' + id,'')
        }
        else {
            var elem = [];
            elem.push({
                type:'text',
                name:'servername',
                label:'Server Name',
                placeholder:'Server Name',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'serveraddress',
                label:'Server Address',
                placeholder:'Server Address',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'username',
                label:'User Name',
                placeholder:'User Name',
                value:'', 
            })

            elem.push({
                type:'password',
                name:'pwd',
                label:'Password',
                placeholder:'Password',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'token',
                label:'Token',
                placeholder:'Token',
                value:'', 
            })

            elem.push({
                type:'checkbox',
                name:'is_active',
                label:'Is Active',
                value:1, 
            })

            elem.push({
                type:'text',
                name:'port',
                label:'Port',
                placeholder:'Port',
                value:'', 
            })

            elem.push({
                type:'text',
                name:'noreply',
                label:'No reply account',
                placeholder:'No reply account',
                value:'', 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'lastmodified',
                label:'Last Modified',
                value:new Date(), 
            })

            setElements(elem);
        }
    },[])

    useEffect(()=>{
        if ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        ) 
        {
            var idtemp = id;   
            var elem = [];
            
            var servername = null;
            var serveraddress = null;
            var username = null;
            var pwd = null;
            var token = null;
            var is_active = null;
            var port = null;
            var noreply = null;
            var lastmodified = null;
            
            if (action==='get') {

                servername = stateGet.data[0].servername; 
                serveraddress = stateGet.data[0].serveraddress; 
                username = stateGet.data[0].username;
                pwd = stateGet.data[0].pwd;  
                is_active = stateGet.data[0].is_active;
                token = stateGet.data[0].token;
                port = stateGet.data[0].port;
                noreply = stateGet.data[0].noreply;
                lastmodified = stateGet.data[0].lastmodified;
            }
            if (action==='put') {
                servername = elements[0].value
                serveraddress = elements[1].value
                username = elements[2].value
                pwd = elements[3].value
                token = elements[4].value
                is_active = elements[5].value
                port = elements[6].value
                noreply = elements[7].value
                lastmodified = elements[8].value
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
                servername = elements[0].value
                serveraddress = elements[1].value
                username = elements[2].value
                pwd = elements[3].value
                token = elements[4].value
                is_active = elements[5].value
                port = elements[6].value
                noreply = elements[7].value
                lastmodified = elements[8].value
            }
            
            elem.push({
                type:'text',
                name:'servername',
                label:'Server Name',
                placeholder:'Server Name',
                value:servername, 
            })

            elem.push({
                type:'text',
                name:'serveraddress',
                label:'Server Address',
                placeholder:'Server Address',
                value:serveraddress, 
            })

            elem.push({
                type:'text',
                name:'username',
                label:'User Name',
                placeholder:'User Name',
                value:username, 
            })

            elem.push({
                type:'password',
                name:'pwd',
                label:'Password',
                placeholder:'Password',
                value:pwd, 
            })

            elem.push({
                type:'text',
                name:'token',
                label:'Token',
                placeholder:'Token',
                value:token, 
            })

            elem.push({
                type:'checkbox',
                name:'is_active',
                label:'Is Active',
                value:is_active, 
            })

            elem.push({
                type:'text',
                name:'port',
                label:'Port',
                placeholder:'Port',
                value:port, 
            })

            elem.push({
                type:'text',
                name:'noreply',
                label:'No reply account',
                placeholder:'No reply account',
                value:noreply, 
            })

            elem.push({
                type:'label',
                datatype:'DATETIME',
                name:'lastmodified',
                label:'Last Modified',
                value:lastmodified, 
            })

            setIDState(idtemp);
            setElements(elem);
            if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
        }
        else {
            var msg = '';
            if (action==='get') msg = stateGet.errorMessage;
            if (action==='put') msg = statePut.errorMessage;
            if (action==='post') msg = statePost.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const onYes = () => {
        var data = {
            servername : elements[0].value,
            serveraddress : elements[1].value,
            username : elements[2].value,
            pwd : elements[3].value,
            token : elements[4].value,
            is_active : elements[5].value,
            port : elements[6].value,
            noreply : elements[7].value
        };
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + '/editmailserver/' + idstate);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + '/createmailserver');
        }
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    return (
        <Container>
            <HeaderEnhancedx title='Mail Server' text='Mail Server detail' />
            <LoadingBox2 open={openload}/>
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={2}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={'Berikut adalah detail mail server. Pastikan detail info sudah terisi dengan benar dan lengkap.'}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default MailServerDetail;
