import React from 'react';
import { 
    Container,
    Tab,

} from 'semantic-ui-react';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import RoleDetail from './Roles/RoleDetail';
import Members from './Roles/Members';
import ExcludeHRMembers from './Roles/ExcludeHRMembers';
import HRMembers from './Roles/HRMembers';
import RoleTopics from './Roles/RoleTopics';
import RoleMenus from './Roles/RoleMenus';



const RoleTab = props =>  {
    const panes = [
        { menuItem: { key: 'detail', content :'Detail' }, render: () => <Tab.Pane><RoleDetail mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
        { menuItem: { key: 'hrmembers', content :'HR Members' }, render: () => <Tab.Pane><HRMembers mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
        { menuItem: { key: 'excludehrmembers', content :'Exclude HR Members' }, render: () => <Tab.Pane><ExcludeHRMembers mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
        { menuItem: { key: 'members', content :'Members' }, render: () => <Tab.Pane><Members mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
        { menuItem: { key: 'topics', content :'Topics' }, render: () => <Tab.Pane><RoleTopics mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
        { menuItem: { key: 'menus', content :'Menus' }, render: () => <Tab.Pane><RoleMenus mode={props.match.params.mode} id={props.match.params.id}/></Tab.Pane> },
    ]
      
    return (
        <Container>
            <HeaderEnhancedx title='Role' text='Detail Role' />
            <p/>
            <Tab menu={{ borderless: true, attached: false, tabular: false, fluid: true }} panes={panes} />
        </Container>
    );
    
}

export default RoleTab;

//    { menuItem: { key: 'errorlogs', content :'Error Logs' }, render: () => <Tab.Pane><ErrorLogs/></Tab.Pane> },
    