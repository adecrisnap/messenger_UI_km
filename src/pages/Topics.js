import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhancedx from '../components/HeaderEnhancedx';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const Topics = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/gettopics/-1');
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : '_id', columncaption : ' ', dataType : 'String', width : 5, link : 'topicdetail/edit', visible : false})
            columns.push({columnname : 'topicname', columncaption : 'Topic Name', dataType: 'String', width: 12});
            columns.push({columnname : 'topiccode', columncaption : 'Code', dataType: 'String', width: 4});
            columns.push({columnname : 'emailsuccess', columncaption : 'Status', dataType: 'Custom', width: 4, warningvalue : 'F', customvalues : [ {value : 'Y', text : 'Success'}, {value: 'N', text :'Non active'}, {value: 'Z', text :'Zero row'}, {value: 'F', text :'Failed'}]});
            columns.push({columnname : 'is_active', columncaption : 'Is Active', dataType: 'Custom', width: 3, customvalues : [ {value : 0, text : 'Non active'}, {value: 1, text :'Active'}]});
            columns.push({columnname : 'emaillastrun', columncaption : 'Last Run', dataType: 'DateTime', width: 5});
            columns.push({columnname : 'startdate', columncaption : 'Start Date', dataType: 'DateTime', width: 5});
            columns.push({columnname : 'lastmodified', columncaption : 'Last Modified', dataType: 'DateTime', width: 5});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhancedx title='Topics' text='All topics registered' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <GridTable
                                    datasource={dataSource} 
                                    columns={columns}
                                    rowsperpage={10}
                                    sortedby={'topicname'}
                                    linkinrowlevel={true}
                                    link={'/topicdetail/add/0'}
                                    
                                    ascdesc={'ascending'}
                                    showaddnew={true}
                                    showaddnewonempty={false}
                                    emptycaption="You don't have any topic"
                                    emptyimage={null}
                                    emptytitle="No topic"
                                    emptybuttoncaption="Add new topic"
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                )
            }
        </Container>
    )
}

export default Topics;

/*

 (stateGet.status!==0 && stateGet.data.length===0) ? (
                        <EmptySpace
                            title="No topic"
                            caption="You don't have any topic"
                            showButton={false}
                            buttonCaption = "Create new topic"
                            onClick={handleClick} 
                        />
                    )
                    : 


*/