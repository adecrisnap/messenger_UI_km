import { useState, useEffect, useReducer } from 'react'
import Helper from './Helper'

const dataFetchReducer = (statePut, action) => {
    switch (action.action.type) {
      case 'FETCH_INIT':
        return {
          ...statePut,
          isLoading: true,
          hasError: false
        };
      case 'FETCH_SUCCESS':
        return {
          ...statePut,
          isLoading: false,
          hasError: false,
          data: action.action.data,
          status : action.action.status,
          randomstatus : action.action.randomstatus
        };
      case 'FETCH_FAILURE':
        return {
          ...statePut,
          isLoading: false,
          hasError: true,
          errorMessage : action.action.errorMessage,
          status : action.action.status,
          randomstatus : action.action.randomstatus
        };
      default:
        throw new Error();
    }
};

const usePutData = (initialUrl, initialPutload, token) => {
    const [url, setPutUrl] = useState(initialUrl)
    const [ refresh, setRefresh ] = useState(false)
    const [putload, setPutload] = useState(initialPutload)
    const [statePut, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        hasError: false,
        data: null,
        errorMessage : '',
        status : 0,
        randomstatus : 0
    });
    
    useEffect(() => {   
        const putData = () => {
            var action = 
            { 
              type : 'FETCH_INIT',
              errorMessage : '',
              status : 0,
              randomstatus : 0,
              data : null
            }
            dispatch({action});
            var pload = {}
            if (token!=='')
              pload = {
                method: "PUT",
                  headers: {
                      'Authorization' : token,
                      'Content-Type' : 'application/json'
                  },
                  body: JSON.stringify(putload),
              }
            else 
              pload = {
                method: "PUT",
                  headers: {
                      'Content-Type' : 'application/json'
                  },
                  body: JSON.stringify(putload),
              }
            return fetch(url, pload)
            .then(r =>  r.json().then((data) => {
                if (r.status!==200 && r.status!==201 && r.status!==304) {
                    var action = 
                    { 
                      type : 'FETCH_FAILURE',
                      errorMessage : 'Error',
                      status : r.status,
                      randomstatus : Helper.makeid(),
                      data : null
                    }
                    dispatch({ action });
                }
                else {
                    var action = 
                    { 
                      type: 'FETCH_SUCCESS', 
                      data: data, 
                      status : r.status,
                      randomstatus : Helper.makeid(),
                      errorMessage : ''
                    }
                    dispatch({action});
                }
            }))
            .catch((error)=>{
                var action = 
                { 
                  type: 'FETCH_FAILURE', 
                  errorMessage : error.message, 
                  status : 500,
                  randomstatus : Helper.makeid(),
                  data : null
                }
                dispatch({action});
            })
        }
        if ((url!==null && putload!==null) || refresh===true)
        putData()
    }, [url, putload, refresh])
    
    return { statePut, setPutUrl, setPutload, setRefresh }
}

export default usePutData