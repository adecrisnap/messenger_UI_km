import { useState, useEffect, useReducer } from 'react'
import URLLocation from './URLLocation';

const dataFetchReducer = (state, action) => {
    switch (action.type) {
      case 'FETCH_INIT':
        return {
          ...state,
          isLoading: true,
          hasError: false
        };
      case 'FETCH_SUCCESS':
        return {
          ...state,
          isLoading: false,
          hasError: false,
          data: action.data,
        };
      case 'FETCH_FAILURE':
        return {
          ...state,
          isLoading: false,
          hasError: true,
          errorMessage : action.errorMessage
        };
      default:
        throw new Error();
    }
};

const usePutData = (initialUrl, initialPayload = {}, token) => {
    const [url, setUrl] = useState(URLLocation + '/' + initialUrl)
    const [payload, setPayload] = useState(initialPayload)
    const [state, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        hasError: false,
        data: initialData,
        errorMessage : ''
    });
    
    useEffect(() => {   
        const postData = () => {
            dispatch({ type: 'FETCH_INIT' });
            return fetch(url, {
                method: "PUT",
                headers: {
                    'Authorization' : token,
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify(payload),
            })
            .then(r =>  r.json().then((data) => {
                if (r.status!==200 || r.status!==201) {
                    dispatch({ type: 'FETCH_FAILURE' , errorMessage : data.message});
                }
                else {
                    dispatch({ type: 'FETCH_SUCCESS', data: data });
                }
            }))
            .catch((error)=>{
                dispatch({ type: 'FETCH_FAILURE' , errorMessage : error.message});
            })
        }
        postData()
    }, [url, payload])
    
    return { state, setUrl, setPayload }
}

export default usePutData