import { useState, useEffect, useReducer } from 'react'
import Helper from './Helper';

const dataFetchReducer =(stateGet, action) => {
    switch (action.action.type) {
      case 'FETCH_INIT':
        return {
          ...stateGet,
          isLoading: true,
          hasError: false
        };
      case 'FETCH_SUCCESS':
        return {
          ...stateGet,
          isLoading: false,
          hasError: false,
          data: action.action.data,
          status : action.action.status,
          randomstatus : action.action.randomstatus
        };
      case 'FETCH_FAILURE':
        return {
          ...stateGet,
          isLoading: false,
          hasError: true,
          errorMessage : action.action.errorMessage,
          status : action.action.status,
          randomstatus : action.action.randomstatus
        };
      default:
        throw new Error();
    }
};

const useGetData = (initialUrl,token) => {
    const [url, setGetUrl] = useState(initialUrl)
    const [refresh, setRefresh] = useState(false)
    const [stateGet, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        hasError: false,
        data: null,
        errorMessage : '',
        status : 0,
        randomstatus : 0
    });
    useEffect(() => {   
        const fetchData = () => {
            var action = 
            { 
              type : 'FETCH_INIT',
              errorMessage : '',
              status : 0,
              data : null,
              randomstatus : 0
            }
            dispatch({action});
            var pload = {}
            if (token!=='') 
              pload =  {
                method: "GET",
                headers: {
                    'Authorization' : token,
                    'Content-Type' : 'application/json'
                },
            }
            else
              pload =  {
                  method: "GET",
                  headers: {
                      'Content-Type' : 'application/json'
                  },
              }
            return fetch(url, pload
            )
            .then(r =>  r.json().then((data) => {
                if (r.status!==200 && r.status!==201 && r.status!==304) {
                    action = { type: 'FETCH_FAILURE' , errorMessage : 'Error', status : r.status, data:null, randomstatus : Helper.makeid()};
                    dispatch({action});
                }
                else {
                 
                    action = 
                    { 
                      type: 'FETCH_SUCCESS', 
                      data: data, 
                      status : r.status, 
                      randomstatus : Helper.makeid(),
                      errorMessage : ''
                    };
                    dispatch({action});
                }
            }))
        }
        if (url!==null && url!=='') {
          if (refresh===true || stateGet.data===null) fetchData()
        }
    }, [url,refresh])
    return { stateGet, setGetUrl, setRefresh }
}

export default useGetData