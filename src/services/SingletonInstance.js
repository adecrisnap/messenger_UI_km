import { useObservable } from 'mobx-react-lite';

const SingletonInstance = useObservable({
    current_id : '',
    current_name : '',
    current_email : '',
    token : '',
    setLogin(current_id,current_email,current_name,token) {
        SingletonInstance.current_id = current_id;
        SingletonInstance.current_email = current_email;
        SingletonInstance.current_name = current_name;
        SingletonInstance.token = token;
    },
    get token() {
        return token;
    },
    get currentID() {
        return current_id;
    },
    get currentName() {
        return current_name;
    },
    get currentEmail() {
        return current_email;
    }
  })

  export default SingletonInstance;


  
/*
//FIXME: USEEFFECT USESTATE, USECONTEXT 
import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";

import "./styles.css";

const Profile = props => {
  const [profileState, setProfileState] = useStateprops;

  useEffect(() => {
    setProfileStateprops;
  }, [props]);

  return (
    <div>
      <p>
        <strong>Name: </strong>
        {profileState.name}
      </p>
      <p>
        <strong>Email: </strong>
        {profileState.email}
      </p>
    </div>
  );
};

const App = () => {
  const [state, setState] = useState({
    name: "Param",
    email: "param@gmail.com"
  });

  const handleChange = () => {
    setState({
      name: "Vennila",
      email: "vennila@gmail.com"
    });
  };

  return (
    <div className="App">
      <Profile {...state} />
      <button onClick={handleChange}>Change Profile</button>
    </div>
  );
};

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);


//FIXME: HOC FACTORY
// This function takes a component...
function withSubscription(WrappedComponent, selectData) {
  // ...and returns another component...
  return class extends React.Component {
    constructorprops {
      superprops;
      this.handleChange = this.handleChange.bind(this);
      this.state = {
        data: selectData(DataSource, props)
      };
    }

    componentDidMount() {
      // ... that takes care of the subscription...
      DataSource.addChangeListener(this.handleChange);
    }

    componentWillUnmount() {
      DataSource.removeChangeListener(this.handleChange);
    }

    handleChange() {
      this.setState({
        data: selectData(DataSource, this.props)
      });
    }

    render() {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      return <WrappedComponent data={this.state.data} {...this.props} />;
    }
  };
}
*/