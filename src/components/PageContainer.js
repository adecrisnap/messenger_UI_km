import React from 'react';
import { Route,Switch } from 'react-router-dom';
import Home from '../pages/Home';
import Databases from '../pages/Databases';
import DatabaseDetail from '../pages/Databases/DatabaseDetail';
import Roles from '../pages/Roles';
import RoleTab from '../pages/RoleTab';
import ReportTab from '../pages/ReportTab';
import MemberDetail from '../pages/Roles/MemberDetail';
import ExcludeMemberDetail from '../pages/Roles/ExcludeMemberDetail';
import Topics from '../pages/Topics';
import TopicDetail from '../pages/Topics/TopicDetail';
import Reports from '../pages/Reports';
import ColumnDetail from '../pages/Reports/ColumnDetail'
import MailServers from '../pages/MailServers';
import MailServerDetail from '../pages/MailServers/MailServerDetail'
import Applications from '../pages/Applications'
import AppTab from '../pages/AppTab'
import MenuDetail from '../pages/Applications/MenuDetail'
import AccessMenuDetail from '../pages/Roles/AccessMenuDetail';
import EmployeeMaster from '../pages/EmployeeMaster';


const PageContainer = props => {
      return (
            <Switch>
                  <Route exact path='/' component={Home} />
                  <Route exact path='/home' component={Home} />
                  <Route exact path='/databases' component={Databases} />
                  <Route exact path='/databasedetail/:mode/:id' component={DatabaseDetail} />
                  <Route exact path='/roles' component={Roles} />
                  <Route exact path='/roletab/:mode/:id' component={RoleTab} />
                  <Route exact path='/reporttab/:mode/:id' component={ReportTab} />
                  <Route exact path='/topicdetail/:mode/:id' component={TopicDetail} />
                  <Route exact path='/memberdetail/:mode/:id' component={MemberDetail} />
                  <Route exact path='/columndetail/:mode/:idreport/:id' component={ColumnDetail} />
                  <Route exact path='/mailservers' component={MailServers} />
                  <Route exact path='/applications' component={Applications} />
                  <Route exact path='/mailserverdetail/:mode/:id' component={MailServerDetail} />
                  <Route exact path='/applicationdetail/:mode/:id' component={AppTab} />
                  <Route exact path='/excludememberdetail/:mode/:id' component={ExcludeMemberDetail} />
                  <Route exact path='/topics' component={Topics} />
                  <Route exact path='/reports' component={Reports} />
                  <Route exact path='/menudetail/:mode/:appid/:menuid' component={MenuDetail} />
                  <Route exact path='/accessmenudetail/:mode/:roleid/:id' component={AccessMenuDetail}/>
                  <Route exact path='/employeesetting' component={EmployeeMaster}/>
            </Switch>
      );

}

export default PageContainer
