import React, {useEffect,useRef, useState} from 'react';
import { 
    Header,
    Segment
} from 'semantic-ui-react';
import './sticky.css'


const HeaderEnhancedx = props => {
    const [isSticky, setSticky] = useState(true);
    const ref = useRef(null);
    
    const handleScroll = () => {
        if (ref.current) {
            setSticky(ref.current.getBoundingClientRect().top <= 0);
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', () => handleScroll);
        };
    }, []);
    
    return (
        <div className={`sticky-wrapper${isSticky ? ' sticky' : ''}`} ref={ref} >
            <p/>
            <p/>
            <Segment className="sticky-inner">
            <Header size='medium' textAlign='left' >
                <Header.Content>
                    {props.title}
                    <Header.Subheader>{props.text}</Header.Subheader>
                </Header.Content>
            </Header>
            </Segment>
            <p/>
            <p/>
            <p/>
        </div>
    )
}

export default HeaderEnhancedx;

/*className={`sticky-wrapper${isSticky ? ' sticky' : ''}`}*/