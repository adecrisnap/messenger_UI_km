import React, { useEffect, useState } from 'react';
import { 
    Grid, 
    Table,
    Pagination,
    Input,
    Button,
    Icon
  } from 'semantic-ui-react';
import Helper from '../services/Helper';
import EmptySpace from './EmptySpace';
import _ from 'lodash'
import "./gridtablestyles.css";
import { useHistory, Link } from "react-router-dom";


function simpleReducer(state, action) {
  var ordby = '';
  if (action.direction==='ascending') 
    ordby = 'asc';
  else
    ordby = 'desc';
  switch (action.type) {
    case 'INITIAL':
      return {
        column: action.column,
        data: _.orderBy(action.data, [action.column],ordby),
        direction: action.direction,
      }
    case 'CHANGE_PAGE':
      return {
        column: state.column,
        data: action.data,
        direction: state.direction,
      }
    case 'CHANGE_SORT':
      return {
        column: action.column,
        data: action.data,
        direction: action.direction,
      }
      /*
        if (state.column === action.column) {
          return {
            ...state,
            data: action.data, //state.data.slice().reverse(),
            direction:
              state.direction === 'ascending' ? 'descending' : 'ascending',
          }
        }
      
      return {
        column: action.column,
        data: _.orderBy(action.data, [action.column]),
        direction: 'ascending',
      }
      */
    default:
      throw new Error()
  }
}

const GridTable = props => {
    let history = useHistory();
    const { 
          datasource, 
          columns,
          rowsperpage,
          sortedby,
          ascdesc,
          link,
          showaddnew,
          linkinrowlevel,
          internalclick,
          showaddnewonempty,
          emptycaption,
          emptyimage,
          emptytitle,
          emptybuttoncaption
        } = props
    const [ numberofpages, setNumberofpages ] = useState(1);
    const [ rowsFiltered, setRowsFiltered ] = useState(datasource);
    const [ activepage, setActivepage] = useState(1); 
    const [ linkindex, setLinkindex ] = useState(-1);
    const [state, dispatch] = React.useReducer(simpleReducer, {
      column: null,
      data: null,
      direction: null,
    })
    const { column, data, direction } = state

    function GetSortOrder(prop) {    
      return function(a, b) {    
          if (a[prop] > b[prop]) {    
              return 1;    
          } else if (a[prop] < b[prop]) {    
              return -1;    
          }    
          return 0;    
      }    
    }    

    function GetSortOrderDesc(prop) {    
      return function(a, b) {    
          if (a[prop] > b[prop]) {    
              return -1;    
          } else if (a[prop] < b[prop]) {    
              return 1;    
          }    
          return 0;    
      }    
    }    

    function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function checkIfLinkExists(columns) {
      var found = -1;
      var i = 0;
      while(found===-1 && i<columns.length){
        if (columns[i].link!==null && columns[i].link!==undefined) found = i;
        i++
      }
      return found;
    }

    function prepareData(datasource) {
      var idx = 0;
      var data = [];
      if (activepage>1) idx = (activepage-1) * rowsperpage;
      if (idx+rowsperpage<=datasource.length) {
        for(var i=idx;i<idx+rowsperpage;i++) {
            data.push(datasource[i]);
        }
      }
      else {
        for(var i=idx;i<datasource.length;i++) {
          data.push(datasource[i]);
        }
      }
      return data;
    }

    useEffect(()=>{
      if (datasource!==null) {
        var ds1 = datasource;
        if (sortedby!==null && sortedby!==undefined) {
          if (ascdesc==='ascending' || ascdesc===null || ascdesc===undefined) 
            ds1 = datasource.sort(GetSortOrder(sortedby));
          else
            ds1 = datasource.sort(GetSortOrderDesc(sortedby));
        }
        var num = Math.ceil(ds1.length/rowsperpage);
        var data = prepareData(ds1)
        setLinkindex(checkIfLinkExists(columns))
        setNumberofpages(num);
        setRowsFiltered(datasource);
        dispatch({ type: 'INITIAL', column: columns[0].columnname , data : data, direction : ascdesc})
      }
    },[datasource])

    useEffect(()=>{
      if (rowsFiltered!==null) {
        var data = prepareData(rowsFiltered);
        dispatch({ type: 'CHANGE_PAGE', data : data})
      }
    },[activepage])
    
    const onPageChange = (e, { activePage }) => {
      setActivepage(activePage);
    }

    const headerCellClick = (columnname) => {
      setActivepage(1)
      var ds1 = null;
      var drt = ''
      if (direction==='ascending') {
        ds1 = datasource.sort(GetSortOrderDesc(columnname));
        drt = 'descending'
      }
      else {
        ds1 = datasource.sort(GetSortOrder(columnname));
        drt = 'ascending'
      }
      var data = prepareData(ds1)
      dispatch({ type: 'CHANGE_SORT', column: columnname , data : data, direction : drt})
    }

    const toTextValue = (value,customvalues) => {
      var found = false;
      var z = 0;
      var txt = '';
      while (found===false && z<customvalues.length) {
        if (customvalues[z].value===value) {
          found = true;
          txt = customvalues[z].text
        }
        z++;
      }
      if (found===true) 
        return txt
      else
        return '' 
    }

    const onSearching = (e, { value }) => {
      setActivepage(1)
      if (datasource!==null) {
        var num = null
        var rowsf = null;
        var data = null;
        if (value!=='') {
          var val = value.toUpperCase();
          rowsf = datasource.filter(function(item) {
            for (var i=0;i<columns.length;i++) {
              if (item[columns[i].columnname]!==undefined && item[columns[i].columnname]!==null)
                if (columns[i].dataType.toUpperCase()==='CUSTOM') {
                  if (toTextValue(item[columns[i].columnname],columns[i].customvalues).toString().toUpperCase().includes(val)) {
                    return true;
                  }
                }
                else {
                  if (item[columns[i].columnname].toString().toUpperCase().includes(val)) {
                    return true;
                  }
                }
            }
            return false;
          });
        }
        else {
          rowsf = datasource
        }
        num = Math.ceil(rowsf.length/rowsperpage);
        data = prepareData(rowsf);
        setNumberofpages(num);
        setRowsFiltered(rowsf)
        dispatch({ type: 'INITIAL', column: columns[0].columnname , data : data, direction : ascdesc})
      }
    }

    const truncateText = (value,width) => {
      if (value!==null && value!==undefined) {
        if (width*6<value.length){
          var val = value.substring(0,width*6) + ' ...';
          return val;
        }
        else {
          return value;
        }
      }
      else 
        return value;
    }

    const formatValue = (value,customvalues) => {
      var ret = value;
      var found = false;
      var i = 0;
      while (found===false && i<customvalues.length) {
        if (value===customvalues[i].value) {
          found = true;
          ret = customvalues[i].text;
        }
        i++;
      }  
      return ret;
    }

    const handleClick = (value) => {
      var val = value.split('|');
      history.push('/' + val[0] + '/' + val[1]);
    }

    const buttonClick = (value) => {
      history.push('/' + value);
    }

    return (
        <Grid>
          {(showaddnew!==null && showaddnew===true) ? 
            (
              <Grid.Row columns={2}>
                  <Grid.Column floated='left' width='1'>
                    <Input icon='search' placeholder='Search...' onChange={onSearching}/>
                  </Grid.Column>
                  <Grid.Column floated='right' width='3'>
                    <Button as={ Link } to={link} icon labelPosition='left' primary size='small'>
                        <Icon name='plus' /> Add new
                    </Button>
                  </Grid.Column>
              </Grid.Row>
            )
          :
            (
              <Grid.Row>
                  <Grid.Column floated='left' width='1'>
                    <Input icon='search' placeholder='Search...' onChange={onSearching}/>
                  </Grid.Column>
              </Grid.Row>
            )
          }
          {(rowsFiltered!==null && rowsFiltered.length>0) ? 
          (
            <>
              <Grid.Row columns={1}>
                <Grid.Column>
                  {(data!==null && data.length>0) ? (
                    <Table singleLine sortable size='small' selectable compact>
                      <Table.Header>
                        <Table.Row>
                        {(columns).map((q) => (
                          (q.visible===null || q.visible===undefined || q.visible===true) ? (
                          <Table.HeaderCell
                            width={q.width}
                            sorted={column === q.columnname ? direction : null}
                            onClick={() => headerCellClick(q.columnname)}
                          >{q.columncaption}</Table.HeaderCell>
                          ) : (
                            null
                          )
                        ))}
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>
                        {(data).map((dr) => 
                          (linkindex>-1 && linkinrowlevel!==null  && linkinrowlevel!==undefined && linkinrowlevel===true) ? 
                          (
                            <Table.Row
                              key={dr[columns[linkindex].columnname]}
                              onClick={()=>handleClick(columns[linkindex].link + '|' + dr[columns[linkindex].columnname])}
                            >
                              {(columns).map((c) => {
                                  if (c.visible===null || c.visible===undefined || c.visible===true) {
                                    var error = false;
                                    if (c.warningvalue!==undefined && c.warningvalue!==null) {
                                      if (dr[c.columnname]===c.warningvalue) error = true;
                                    }   
                                    
                                    switch(c.dataType.toUpperCase()) {
                                      case 'DATE':
                                        return <Table.Cell error={error} width={c.width}>{Helper.formatDateShort(dr[c.columnname])}</Table.Cell>
                                        break;
                                      case 'DATETIME':
                                        return <Table.Cell error={error} width={c.width}>{Helper.formatDate(dr[c.columnname])}</Table.Cell>
                                        break;
                                      case 'NUMBER':
                                          return <Table.Cell textAlign='right' error={error} width={c.width}>{numberWithCommas(dr[c.columnname])}</Table.Cell>
                                          break;
                                      case 'CUSTOM':
                                        return <Table.Cell error={error} width={c.width}>{formatValue(dr[c.columnname],c.customvalues)}</Table.Cell>
                                        break;
                                      case 'BUTTON':
                                        return <Table.Cell error={error} width={c.width}><Button onClick={()=>{ (internalclick===true) ? buttonClick(c.link + '/' + dr[c.columnname]) : props.buttonClick(dr[c.columnname],c.action)}} size='tiny'>{c.buttonCaption}</Button></Table.Cell>
                                        break;
                                      default:
                                        return <Table.Cell error={error} width={c.width}>{truncateText(dr[c.columnname], c.width)}</Table.Cell>
                                        break;
                                    }
                                  }
                                })
                              }
                            </Table.Row>
                          ) : 
                          (
                            <Table.Row>
                              {(columns).map((c) => {
                                  if (c.visible===null || c.visible===undefined || c.visible===true) {
                                    var error = false;
                                    if (c.warningvalue!==undefined && c.warningvalue!==null) {
                                      if (dr[c.columnname]===c.warningvalue) error = true;
                                    }   
                                    switch(c.dataType.toUpperCase()) {
                                      case 'DATE':
                                        return <Table.Cell error={error} width={c.width}>{Helper.formatDateShort(dr[c.columnname])}</Table.Cell>
                                        break;
                                      case 'DATETIME':
                                        return <Table.Cell error={error} width={c.width}>{Helper.formatDate(dr[c.columnname])}</Table.Cell>
                                        break;
                                      case 'NUMBER':
                                          return <Table.Cell textAlign='right' error={error} width={c.width}>{numberWithCommas(dr[c.columnname])}</Table.Cell>
                                          break;
                                      case 'CUSTOM':
                                        return <Table.Cell error={error} width={c.width}>{formatValue(dr[c.columnname],c.customvalues)}</Table.Cell>
                                        break;
                                        case 'BUTTON':
                                          return <Table.Cell error={error} width={c.width}><Button onClick={()=>{ (internalclick===true) ? buttonClick(c.link + '/' + dr[c.columnname]) : props.buttonClick(dr[c.columnname],c.action)}} size='tiny'>{c.buttonCaption}</Button></Table.Cell>
                                          break;
                                      default:
                                        return <Table.Cell error={error} width={c.width}>{truncateText(dr[c.columnname],c.width)}</Table.Cell>
                                        break;
                                    }
                                  }
                                })
                              }
                            </Table.Row>
                          )
                        )}
                      </Table.Body>
                      <Table.Footer>
                        <Table.Row>
                          <Table.HeaderCell>
                            <p/>
                            <Pagination
                                floated='right'
                                activePage={activepage}
                                onPageChange={onPageChange}
                                totalPages={numberofpages}
                            />
                             <p/>
                            <label>{(datasource!==null) ? 'Showing ' + (activepage*rowsperpage-rowsperpage+1) + ' to ' + ((activepage*rowsperpage>rowsFiltered.length) ? rowsFiltered.length : activepage*rowsperpage) + ' of ' + (rowsFiltered.length) + ' entries' : ''}</label>
                          </Table.HeaderCell>
                        </Table.Row>
                      </Table.Footer>
                    </Table>
                  ) 
                  :
                  (
                    <EmptySpace
                        title={(emptytitle!==null && emptytitle!==undefined) ? emptytitle : 'No result is found'}
                        caption={(emptycaption!==null && emptycaption!==undefined) ? emptycaption : 'No result is found'}
                        showButton={(showaddnewonempty!==null && showaddnewonempty!==undefined) ? showaddnewonempty : false}
                        image={(emptyimage!==null && emptyimage!==undefined) ? emptyimage : null}
                        buttonCaption={(emptybuttoncaption!==null && emptybuttoncaption!==undefined) ? emptybuttoncaption : 'Add new'}
                        link={link}
                    />
                  )
                }
                </Grid.Column>
              </Grid.Row>
            </>
          )
          : 
          (
            <Grid.Row columns={1}>
              <Grid.Column>
                <EmptySpace
                    title={(emptytitle!==null && emptytitle!==undefined) ? emptytitle : 'No result is found'}
                    caption={(emptycaption!==null && emptycaption!==undefined) ? emptycaption : 'No result is found'}
                    showButton={(showaddnewonempty!==null && showaddnewonempty!==undefined) ? showaddnewonempty : false}
                    image={(emptyimage!==null && emptyimage!==undefined) ? emptyimage : null}
                    buttonCaption={(emptybuttoncaption!==null && emptybuttoncaption!==undefined) ? emptybuttoncaption : 'Add new'}
                    link={link}
                />
              </Grid.Column>
            </Grid.Row>
          )}
        </Grid>
    )
}

export default GridTable;
