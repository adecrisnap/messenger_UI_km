import React from 'react';
import { 
    Header,
} from 'semantic-ui-react';


const HeaderEnhanced = props => {
   
    return (
        <div>
            <p/>
            <p/>
            <Header size='medium' textAlign='left'>
                    {props.title}
                    <Header.Subheader>{props.text}</Header.Subheader>
                    
            </Header>
            <p/>
            <p/>
            <p/>
        </div>
    )
}

export default HeaderEnhanced;

/*className={`sticky-wrapper${isSticky ? ' sticky' : ''}`}*/