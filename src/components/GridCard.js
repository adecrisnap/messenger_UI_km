import React, { useState, useEffect } from "react"
import EmptySpace from './EmptySpace';
import { 
    Card, 
    Image, 
    Container, 
    Grid, 
    Pagination,
    Button,
    Dropdown,
    Icon,
    Input,
} from "semantic-ui-react"
import { useHistory, Link } from "react-router-dom";
import _ from 'lodash'

function simpleReducer(state, action) {
    var ordby = '';
    if (action.direction==='ascending') 
        ordby = 'asc';
    else
        ordby = 'desc';
    switch (action.type) {
        case 'INITIAL':
            return {
                column: action.column,
                data: _.orderBy(action.data, [action.column],ordby),
                direction: action.direction,
            }
        case 'CHANGE_SORT':
            if (state.column === action.column) {
                return {
                    ...state,
                    data: state.data.slice().reverse(),
                    direction:
                    state.direction === 'ascending' ? 'descending' : 'ascending',
                }
            }
    
            return {
                column: action.column,
                data: _.orderBy(action.data, [action.column]),
                direction: 'ascending',
            }
        default:
            throw new Error()
    }
}

const SingleCard = ({ imageUrl, title, meta, description, onClick, footer }) => {
    return (
      <Card onClick={onClick} fluid>
        <Image src={imageUrl} wrapped ui={false} />
        <Card.Content>
            <Card.Header>{title}</Card.Header>
            <Card.Meta>
                <span className='date'>{meta}</span>
            </Card.Meta>
            <Card.Description>
                {description}
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            <a>
                {footer}
            </a>
        </Card.Content>
      </Card>
    )
}

const GridCard = props => {
    let history = useHistory();
    const { 
        datasource, 
        recordsperpage,
        columnsnumber,
        sortedby,
        ascdesc,
        link,
        showaddnew,
        showaddnewonempty,
        emptycaption,
        emptyimage,
        emptytitle,
        emptybuttoncaption,
        optionssortby
    } = props

    const [ ds, setDs] = useState(null);
    const [ numberofpages, setNumberofpages ] = useState(1);
    const [ activepage, setActivepage] = useState(1); 
    const [state, dispatch] = React.useReducer(simpleReducer, {
        column: null,
        data: null,
        direction: null,
      })
    const { column, data, direction } = state

    function GetSortOrder(prop) {    
        return function(a, b) {    
            if (a[prop] > b[prop]) {    
                return 1;    
            } else if (a[prop] < b[prop]) {    
                return -1;    
            }    
            return 0;    
        }    
    }    
  
    function GetSortOrderDesc(prop) {    
        return function(a, b) {    
            if (a[prop] > b[prop]) {    
                return -1;    
            } else if (a[prop] < b[prop]) {    
                return 1;    
            }    
            return 0;    
        }    
    }   
    
    useEffect(()=>{
        if (datasource!==null) {
        const numberofpages = Math.ceil(datasource.length/recordsperpage);
        var data = [];
        var ds1 = datasource;
        if (sortedby!==null && sortedby!==undefined) {
          if (ascdesc==='ascending' || ascdesc===null || ascdesc===undefined) 
            ds1 = datasource.sort(GetSortOrder(sortedby));
          else
            ds1 = datasource.sort(GetSortOrderDesc(sortedby));
        }
        var idx = activepage * numberofpages;
        if (idx+recordsperpage<=ds1.length) {
            for(var i=idx;i<idx+recordsperpage;i++) {
                data.push(ds1[i]);
            }
        }
        else {
            for(var i=idx;i<ds1.length;i++) {
                data.push(ds1[i]);
            }
        }
        setDs(data)
        setNumberofpages(numberofpages);
        //dispatch({ type: 'INITIAL', column: columns[0].columnname , data : data, direction : ascdesc})
    }
    },[datasource,activepage])

    const onPageChange = (e, { activePage }) => {
        setActivepage(activePage);
    }

    const dropdownClick = (e,{value}) => {

    }

    return(
        <Grid>
            {(showaddnew!==null && showaddnew===true) ? 
                (
                    <Grid.Row columns={2}>
                        <Grid.Column floated='left' width='1'>
                            <Input icon='search' placeholder='Search...' onChange={props.onSearching}/>
                        </Grid.Column>
                        <Grid.Column floated='right' width='3'>
                            <Dropdown 
                                placeholder='Sort by'
                                fluid
                                selections
                                options={optionssortby}
                                onClick={dropdownClick}
                            />
                        </Grid.Column>
                        <Grid.Column floated='right' width='3'>
                            <Button as={ Link } to={link} icon labelPosition='left' primary size='small'>
                                <Icon name='plus' /> Add new
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                )
            :
                (
                    <Grid.Row>
                        <Grid.Column floated='left' width='1'>
                            <Input icon='search' placeholder='Search...' onChange={props.onSearching}/>
                        </Grid.Column>
                        <Grid.Column floated='right' width='3'>
                            <Dropdown 
                                placeholder='Sort by'
                                fluid
                                selections
                                options={optionssortby}
                                onClick={dropdownClick}
                            />
                        </Grid.Column>
                    </Grid.Row>
                )
            }
            {(datasource!==null && datasource.length>0) ? 
            (
                <>
                    <Grid.Row>
                        <Grid.Column>
                            <Container className="container" textAlign="center">
                                <Card.Group itemsPerRow={columnsnumber} stackable={true}>
                                    {ds &&
                                        ds.map(obj => {
                                        return (
                                            <SingleCard
                                                title={obj.header}
                                                meta={obj.subheader} 
                                                description={obj.description}
                                                onClick={() => {props.cardClick(obj.id,obj.action)}}
                                                imageUrl={(obj.img) ? obj.img : null}
                                            />
                                        )
                                    })}
                                </Card.Group>
                            </Container>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                        <p/>
                        <Pagination
                            floated='right'
                            activePage={activepage}
                            onPageChange={onPageChange}
                            totalPages={numberofpages}
                        />
                            <p/>
                        <label>{(datasource!==null) ? 'Showing ' + (activepage*recordsperpage-recordsperpage+1) + ' to ' + ((activepage*recordsperpage>datasource.length) ? datasource.length : activepage*recordsperpage) + ' of ' + (datasource.length) + ' entries' : ''}</label>
                        </Grid.Column>
                    </Grid.Row>
                </>
            )
            : (
                <Grid.Row>
                    <Grid.Column>
                        <EmptySpace
                            title={(emptytitle!==null && emptytitle!==undefined) ? emptytitle : 'No result is found'}
                            caption={(emptycaption!==null && emptycaption!==undefined) ? emptycaption : 'No result is found'}
                            showButton={(showaddnewonempty!==null && showaddnewonempty!==undefined) ? showaddnewonempty : false}
                            image={(emptyimage!==null && emptyimage!==undefined) ? emptyimage : null}
                            buttonCaption={(emptybuttoncaption!==null && emptybuttoncaption!==undefined) ? emptybuttoncaption : 'Add new'}
                            link={link}
                        />
                    </Grid.Column>
                </Grid.Row>
            )}
        </Grid>
    )
}

export default GridCard;