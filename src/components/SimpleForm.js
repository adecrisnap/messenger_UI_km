import React from 'react';
import {
    Container,
    Button,
    Form,
    Grid,
    Segment,
} from 'semantic-ui-react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Helper from '../services/Helper';
//import "./simpleform.css";


const SimpleForm = props => {
    const {
        elements,
        yesCaption,
        noCaption,
        numberofcolumns,
        label,
        customButtonOne,
        customButtonTwo,
    } = props
    var numcol = 1;
    var comp = [];
    var numofelements = elements.length;
    var numofelementsdisplayed = 0;
    var numofelementsdisplayedfinal = 0;

    const formatValue = (value,customvalues) => {
        var ret = value;
        var found = false;
        var i = 0;
        while (found===false && i<customvalues.length) {
          if (value===customvalues[i].value) {
            found = true;
            ret = customvalues[i].text;
          }
          i++;
        }  
        return ret;
    }

    return (
        <Container>
            <Segment textAlign='left' basic>
                <p/>
                <Grid style={{ marginLeft: "2em"}}>
                    <Grid.Row>
                        <Grid.Column width='8'>
                            <label>{label}</label>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Form style={{ marginTop: "2em",marginBottom: "2em"}}>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width='1'>&nbsp;</Grid.Column>
                            <Grid.Column width='11' >
                                {
                                    (elements).map((q) => {
                                        numofelementsdisplayed++;
                                        var c = null;
                                        switch(q.type) {
                                            case "text":
                                                c =
                                                    <Form.Input 
                                                        name={q.name}
                                                        fluid 
                                                        label={q.label} 
                                                        placeholder={q.placeholder}
                                                        onChange={props.onChange}
                                                        value={q.value}
                                                    />
                                                break;
                                            case "button":
                                                c =
                                                    <Button
                                                        fluid
                                                        onClick={()=>props.buttonClick(q.value)}
                                                    >
                                                        {q.buttonCaption}
                                                    </Button>
                                                break;
                                            case "memo":
                                                c =
                                                    <Form.TextArea 
                                                        name={q.name}
                                                        fluid
                                                        label={q.label} 
                                                        placeholder={q.placeholder} 
                                                        onChange={props.onChange}
                                                        value={q.value}
                                                    />
                                                break;
                                            case "checkbox":
                                                c = 
                                                    <Form.Checkbox 
                                                        label={q.label}
                                                        name={q.name}
                                                        value={q.value}
                                                        toggle
                                                        checked={((q.value===1) ? true : false)}
                                                        onChange={(e, data) => props.onCheckChange(q.name, data.value)}
                                                        
                                                    />
                                                break;
                                            case "password":
                                                c =
                                                    <Form.Input 
                                                        name={q.name}
                                                        fluid 
                                                        label={q.label} 
                                                        placeholder={q.placeholder}
                                                        type='password'
                                                        onChange={props.onChange}
                                                        value={q.value}
                                                    />
                                                break;
                                            case "calendar":
                                                c = <>
                                                    <label>{q.label} : &nbsp;</label>
                                                    <DatePicker 
                                                        name={q.name}
                                                        selected={(q.selectedDate!==null && q.selectedDate!==undefined) ? new Date(q.selectedDate) : new Date()} 
                                                        onChange={(date) => props.onDateChange(q.name, date)}
                                                        showTimeSelect
                                                        timeFormat="HH:mm"
                                                        timeIntervals={15}
                                                        timeCaption="time"
                                                        dateFormat="MMMM d, yyyy h:mm aa"
                                                    />
                                                    </>
                                                break;    
                                            case "dropdown":
                                                c =
                                                    <Form.Dropdown
                                                        name={q.name}
                                                        label={q.label}
                                                        fluid
                                                        onChange={(e, data) => props.onDropdownChange(q.name, data.value)}
                                                        options={q.options}
                                                        selection
                                                        search
                                                        multiple={q.multiple}
                                                        value={q.value}
                                                    />
                                                break;
                                            case "label":
                                                switch(q.datatype) {
                                                    case "DATE":
                                                        c =
                                                            <div textAlign='right'>
                                                            <label>{q.label + ' ' + Helper.formatDateShort(q.value)}</label>
                                                            </div>
                                                        break;
                                                    case "DATETIME":
                                                        c =
                                                            <div textAlign='right'>
                                                            <label>{q.label + ' ' + Helper.formatDate(q.value)}</label>
                                                            </div>
                                                        break;
                                                    case "CUSTOM":
                                                        c =
                                                            <div textAlign='right'>
                                                            <label>{q.label + ' ' + formatValue(q.value)}</label>
                                                            </div>
                                                        break;
                                                    default:
                                                        c =
                                                            <div textAlign='right'>
                                                            <label>{q.label + ' ' + q.value}</label>
                                                            </div>
                                                        break;
                                                }
                                        }
                                        comp.push(c)
                                        if (numcol===numberofcolumns) {
                                            numofelementsdisplayedfinal = numofelementsdisplayed;
                                            numcol = 1;
                                            var compx = comp
                                            comp = [];
                                            return <Form.Group widths='equal'>{compx}</Form.Group>
                                        }
                                        else {
                                            if (numofelementsdisplayed===numofelements && numofelementsdisplayedfinal<numofelements) {
                                                var compx = comp
                                                comp = [];
                                                return <Form.Group widths='equal'>{compx}</Form.Group>
                                            }
                                            numcol++;
                                        }
                                    }
                                )}
                            </Grid.Column>
                            <Grid.Column width='4' className="sticky-inner">
                                <Button
                                    content={yesCaption}
                                    labelPosition='right'
                                    fluid
                                    icon='checkmark'
                                    onClick={props.onYes}
                                    positive
                                /> 
                                <p/>
                                <Button onClick={props.onClose} fluid>
                                    {noCaption}
                                </Button>
                                <p/>
                                <p/>
                                {(customButtonOne!==null && customButtonOne!==undefined)? 
                                    (
                                        <Button
                                            onClick={props.customButtonOneClick}
                                            fluid
                                        >
                                            {customButtonOne}
                                        </Button>  
                                    ) 
                                    : 
                                    null
                                }
                                <p/>
                                <p/>
                                {(customButtonTwo!==null && customButtonTwo!==undefined)? 
                                    (
                                        <Button
                                            onClick={props.customButtonTwoClick}
                                            fluid
                                        >
                                            {customButtonTwo}
                                        </Button>  
                                    ) 
                                    : 
                                    null
                                }
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form>  
            </Segment>
        </Container>
    )
}

export default SimpleForm
/* marginBottom: "5em"*/