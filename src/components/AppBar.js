import React, { useState} from 'react';
import {
  Menu,
  Container,
  Popup,
  Grid,
  Segment,
  Icon,
  Image,
  Dropdown,
  List,
  
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Sidebar from "react-sidebar";
import { createMedia } from "@artsy/fresnel"

const { MediaContextProvider, Media } = createMedia({
  breakpoints: {
    sm: 0,
    md: 768,
    lg: 1024,
    xl: 1192,
  },
})

const sidebarcontent = 
    <div>
        <Segment compact>
            <List divided>
                <List.Item>
                    <List.Content>
                        <List.Header as={Link} to={'/applications/'}>Applications</List.Header>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <List.Content>
                        <List.Header as={Link} to={'/roles/'}>Roles</List.Header>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <List.Content>
                        <List.Header as={Link} to={'/reports/'}>Reports</List.Header>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <List.Content>
                        <List.Header as={Link} to={'/topics/'}>Topics</List.Header>
                    </List.Content>
                </List.Item>
               
            </List>
        </Segment>
    </div>

const NavBarMobile = props => {
    const [ open, setOpen ] = useState(false)
    const [ sidebarOpen, setSidebarOpen ] = useState(true)
    const [ docked, setDocked ] = useState(true)
    const { children } = props
    
    const handleCancel = () => {
        setOpen(false)
    }

  
    const logOut = () => 
    {
        setOpen(true)
    }

    const accountClick = () =>
    {
        
    }

    const setSidebar = () => 
    {
        setDocked(!docked)
        setSidebarOpen(!sidebarOpen)
    }
    var accountbutton =  <b><Dropdown.Item  as={Link} onClick={accountClick} icon='user' text='Account Settings' /></b>
    var logoutbutton =  <b><Dropdown.Item  as={Link} onClick={logOut} icon='sign-out' text='Sign Out' /></b>

    //menu item
        var home = <b><Dropdown.Item as={Link} to={'/home/'} text='Home' /></b>
        var item_container_1_report_1 = <b><Dropdown.Item as={Link} to={'/databases/'} text='Database Servers' /></b>
        var item_container_1_report_2 = <b><Dropdown.Item as={Link} to={'/mailservers/'} text='Mail Servers' /></b>
        var item_container_1_report_3 = <b><Dropdown.Item as={Link} to={'/employeesetting/'} text='Employee Master Setting' /></b>
      

    //group menu
        var group_general = 
            <Dropdown text='More'>
                <Dropdown.Menu>
                    {home}
                    {item_container_1_report_1}
                    {item_container_1_report_2}
                    {item_container_1_report_3}
                </Dropdown.Menu>
            </Dropdown> 

    //menu container 
        var menu_container = 
            <Menu.Item as='a'>
                {group_general}
            </Menu.Item>

    return (
        <React.Fragment>
            <Sidebar
                sidebar={sidebarcontent}
                open={sidebarOpen}
                docked={docked}
                shadow={false}
                styles={{ sidebar: { background: "whitesmoke",marginTop: "4.5em" } }}
            >
                <Menu borderless size='small' fixed="top" pointing secondary>
                    <Menu.Item onClick={setSidebar}>
                        <Icon name='sidebar'/>
                    </Menu.Item>
                    <Menu.Item>
                        <Image size="mini" src="https://react.semantic-ui.com/logo.png" />
                    </Menu.Item>
                   
                    <Menu.Menu position="right">
                        {menu_container}
                        <Menu.Item>
                            <Popup position='bottom right' flowing hoverable on='click' trigger={
                                <Dropdown as='a' icon='user' text='Welcome&nbsp;' />
                            }>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column>
                                            &nbsp;&nbsp;&nbsp;{accountbutton}
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                            &nbsp;&nbsp;&nbsp;{logoutbutton}
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Popup>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container style={{ marginTop: "7em"}}>{children}</Container>
            </Sidebar>
        </React.Fragment>
    );
}

const NavBarDesktop = props => {
    const [ open, setOpen ] = useState(false)
    const { children } = props
    const [ sidebarOpen, setSidebarOpen ] = useState(true)
    const [ docked, setDocked ] = useState(true)
   

    const handleCancel = () => {
        setOpen(false)
    }

    const logOut = () => 
    {
        setOpen(true)
    }

    const accountClick = () =>
    {

    }

    const setSidebar = () => 
    {
        setDocked(!docked)
        setSidebarOpen(!sidebarOpen)
    }

    //item menus
    var item_container_1_report_1 = <b><Dropdown.Item as={Link} to={'/databases/'} text='Database Servers' /></b>
    var item_container_1_report_2 = <b><Dropdown.Item as={Link} to={'/mailservers/'} text='Mail Servers' /></b>
    var item_container_1_report_3 = <b><Dropdown.Item as={Link} to={'/employeesetting/'} text='Employee Master Setting' /></b>
    

    //group menu
        var group_container_1 = 
            <b><Dropdown text='General'>
                <Dropdown.Menu>
                    {item_container_1_report_1}
                    {item_container_1_report_2}
                    {item_container_1_report_3}
                </Dropdown.Menu>
            </Dropdown></b> 
        
    //menu container 
        var home = 
            <Menu.Item as={Link} to='/home'>
                <b>Home</b>
            </Menu.Item>
        var menu_container_1 = 
            <Menu.Item as='a'>
                {group_container_1}
            </Menu.Item> 
        var accountbutton =  <b><Dropdown.Item  as={Link} onClick={accountClick} icon='user' text='Account Settings' /></b>
        var logoutbutton = <b><Dropdown.Item  as={Link} onClick={logOut} icon='sign-out' text='Sign Out' /></b>

    return (
        <React.Fragment>
            <Sidebar
                sidebar={sidebarcontent}
                open={sidebarOpen}
                docked={docked}
                shadow={false}
                styles={{ sidebar: { background: "whitesmoke",marginTop: "4.5em"  } }}
            >
                <Menu borderless size='small' fixed="top" pointing>
                    <Menu.Item onClick={setSidebar}>
                        <Icon name='sidebar'/>
                    </Menu.Item>
                    <Menu.Item>
                        <Image size="mini" src="https://react.semantic-ui.com/logo.png" />
                    </Menu.Item>
                    <Menu.Menu position="right">
                        {home}
                        {menu_container_1}
                        <Menu.Item>
                            <Popup position='bottom right' flowing hoverable on='click' trigger={
                                <Dropdown as='a' icon='user' text='Welcome&nbsp;' />
                            }>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column>
                                            &nbsp;&nbsp;&nbsp;{accountbutton}
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                            &nbsp;&nbsp;&nbsp;{logoutbutton}
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Popup>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container style={{ marginTop: "7em"}}>{children}</Container>
            </Sidebar>
        </React.Fragment>
    );
}

const NavBar = props => {
    const [ visible, setVisible ] = useState(false)
    const { children } = props

    const handlePusher = () =>{
        if (visible) setVisible(false)
    };

    const handleToggle = () => {
        setVisible(!visible)
    }

    return (
         <MediaContextProvider>
            <Media lessThan="lg">
                <NavBarMobile
                    onPusherClick={handlePusher}
                    onToggle={handleToggle}
                    visible={visible}
                >  
                    {children}
                </NavBarMobile>
            </Media>
            <Media greaterThanOrEqual="lg">
                <NavBarDesktop>
                    {children}
                </NavBarDesktop>
            </Media>
        </MediaContextProvider>
    )
}

const AppBar = props => {
    const { children } = props
    const [ fixed, setFixed ] = useState(true)

    const hideFixedMenu = () => {
        setFixed(false)
    }

    const showFixedMenu = () => {
        setFixed(true)
    }

    return (
        <div>
            <NavBar>{children}</NavBar>
        </div>
    )
  
    
}

AppBar.propTypes = {
  children: PropTypes.node,
}

export default AppBar;


/*

 <List.Item>
                    <List.Content>
                        <List.Header as={Link} to={'/tags/'}>Tags</List.Header>
                    </List.Content>
                </List.Item>


                */